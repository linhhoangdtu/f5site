<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sanpham extends Model
{
    protected $table = 'sanpham';
    protected $fillable=['id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dm','id_dmsp'];
    public $timestamps=false;
    public function danhmuc(){
    	return $this->belongTo('App\Danhmuc');
    }
    public function danhmucsanpham(){
    	return $this->belongTo('App\Danhmucsanpham');
    }
}
