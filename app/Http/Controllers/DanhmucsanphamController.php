<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DanhmucsanphamRequest;
use App\Danhmucsanpham;
use App\Danhmuc;

class DanhmucsanphamController extends Controller
{
    public function getList(){
		$datadmsp = Danhmucsanpham::select('id','tendmsp','id_dm')->orderBy('id','DESC')->get()->toArray();
		return view('admin.danhmucsanpham.list',compact('datadmsp'));
	}
    public function getAdd(){
    	 $cate = Danhmuc::select('id','tendm')->get()->toArray();
    	return view('admin.danhmucsanpham.add',compact('cate'));
    }
    public function postAdd(DanhmucsanphamRequest $Request){
    	$danhmucsanpham = new Danhmucsanpham;
    	$danhmucsanpham->tendmsp = $Request->txtTendmsp;
    	$danhmucsanpham->id_dm = $Request->txtId_dm;
    	$danhmucsanpham->save();
    	return redirect()->route('admin.danhmucsanpham.list')->with(['flash_level'=>'seccess','flash_message'=>'Success !! Complete Add danhmuc san pahm']);
    }
    public function getDelete ($id){
        $danhmucsanpham = Danhmucsanpham::find($id);
        $danhmucsanpham->delete();
        return redirect()->route('admin.danhmucsanpham.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
    public function getEdit ($id){
        $data = Danhmucsanpham::findOrFail($id)->toArray();
        $datadmsp = Danhmucsanpham::select('id','tendmsp','id_dm')->get()->toArray();
        return view('admin.danhmucsanpham.edit',compact('datadmsp','data','id'));
    }
    public function postEdit (Request $Request,$id){
        $this->validate($Request,
        ["txtTendmsp"=>"required"],
        ["txtTendmsp.required"=>"please enter ten danh muc san pham"]
        );
        $danhmucsanpham = Danhmucsanpham::find($id);
        $danhmucsanpham->tendmsp = $Request->txtTendmsp;
        $danhmucsanpham->save();
         return redirect()->route('admin.danhmucsanpham.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
}
