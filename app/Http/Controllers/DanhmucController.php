<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DanhmucRequest;
use App\Danhmuc;
class DanhmucController extends Controller
{
	public function getList(){
		$datadm = Danhmuc::select('id','tendm')->orderBy('id','DESC')->get()->toArray();
		return view('admin.danhmuc.list',compact('datadm'));
	}
    public function getAdd(){
    	return view('admin.danhmuc.add');
    }
    public function postAdd(DanhmucRequest $Request){
    	$danhmuc = new Danhmuc;
    	$danhmuc->tendm = $Request->txtTendm;
    	$danhmuc->save();
    	return redirect()->route('admin.danhmuc.list')->with(['flash_level'=>'seccess','flash_message'=>'Success !! Complete Add danhmuc']);
    }
    public function getDelete ($id){
        $danhmuc = Danhmuc::find($id);
        $danhmuc->delete();
        return redirect()->route('admin.danhmuc.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
    public function getEdit ($id){
        $data = Danhmuc::findOrFail($id)->toArray();
        $datadm = Danhmuc::select('id','tendm')->get()->toArray();
        return view('admin.danhmuc.edit',compact('datadm','data','id'));
    }
    public function postEdit (Request $Request,$id){
        $this->validate($Request,
        ["txtTendm"=>"required"],
        ["txtTendm.required"=>"please enter ten danh muc"]
        );
        $danhmuc = Danhmuc::find($id);
        $danhmuc->tendm = $Request->txtTendm;
        $danhmuc->save();
         return redirect()->route('admin.danhmuc.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
}
