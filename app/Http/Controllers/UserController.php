<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\User;

class UserController extends Controller
{
    public function getList(){
        $datauser = User::select('id','name','email','level')->orderBy('id','DESC')->get()->toArray();
        return view('admin.user.list',compact('datauser'));

    }
    public function getAdd(){
    	return view('admin.user.add');
    }
    public function postAdd(UserRequest $Requestuser){
        $user = new User;
        $user->name = $Requestuser->txtUser;
        $user->email = $Requestuser->txtEmail;
        $user->password = $Requestuser->txtPass;
        $user->level = $Requestuser->rdoLevel;
        $user->save();
        return redirect()->route('admin.user.list')->with(['flash_level'=>'success','flash_message'=>'Đã Thêm thành công 1 cột dữ liệu']);
    }
    public function getDelete(){
    	$user = User::find($id);
        $user->delete();
        return redirect()->route('admin.user.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
    public function getEdit($id){
         $data = User::findOrFail($id)->toArray();
        $datauser = User::select('id','name','email','password','level')->get()->toArray();
        return view('admin.user.edit',compact('data','datauser','id'));
    }
    public function postEdit(){
    	
    }
}
