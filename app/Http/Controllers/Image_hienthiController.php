<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Image_hienthi;
class Image_hienthiController extends Controller
{
    public function getAdd (){
         $cate = Danhmuc::select('id','tendm')->get()->toArray();
             	return view('admin.image_hienthi.add',compact('cate'));
    }
    public function postAdd (Request $Requestha){
        $file_name = $Requestha->file('fImages')->getClientOriginalName();
    	$image_hienthi = new Image_hienthi;
    	$image_hienthi->hinhanh =$file_name;
    	$image_hienthi->id_dm = $Requestha->txtId_dm;
         $Requestha->file('fImages')->move('resources/upload',$file_name);
    	$image_hienthi->save();
    	return redirect()->route('admin.image_hienthi.list')
}
