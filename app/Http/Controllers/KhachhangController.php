<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KhachhangRequest;
use App\Http\Requests\EditkhachhangRequest;
use App\Http\Requests;
use App\Khachhang;

class KhachhangController extends Controller
{
	public function getList(){
		$datakh = Khachhang::select('id','tenkh','matkhau','diachi','sodienthoai')->orderBy('id','DESC')->get()->toArray();
		return view('admin.khachhang.list',compact('datakh'));
    }
    public function getAdd(){
    	return view('admin.khachhang.add');
    }
    
    public function postAdd(KhachhangRequest $Requestkh){
    	$khachhang = new Khachhang;
    	$khachhang->tenkh = $Requestkh->txtTenkh;
    	$khachhang->diachi = $Requestkh->txtDiachi;
    	$khachhang->sodienthoai = $Requestkh->txtSdt;
    	$khachhang->matkhau = $Requestkh->txtPass;
    	$khachhang->save();
    	return redirect()->route('admin.khachhang.list')->with(['flash_level'=>'success','flash_message'=>'Đã Thêm thành công 1 cột dữ liệu']);
    }
    public function getDelete($id){
    	$khachhang = Khachhang::find($id);
    	$khachhang->delete();
    	 return redirect()->route('admin.khachhang.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
    public function getEdit($id){
    	$data = Khachhang::findOrFail($id)->toArray();
    	$datakh = Khachhang::select('id','tenkh','matkhau','diachi','sodienthoai');
    	return view('admin.khachhang.edit',compact('data','datakh','id'));
    }
    public function postEdit(EditkhachhangRequest $Requestkh,$id){
    	$khachhang = Khachhang::find($id);
    	$this->validate($Requestkh,
    		["txtTenkh"=>"required"],
    		["txtTenkh.required"=>"please enter ten khach hang"]
    		);
    	$khachhang->tenkh = $Requestkh->txtTenkh;
    	$khachhang->diachi = $Requestkh->txtDiachi;
    	$khachhang->sodienthoai = $Requestkh->txtSdt;
    	$khachhang->matkhau = $Requestkh->txtPass;
    	$khachhang->save();
    	return redirect()->route('admin.khachhang.list')->with(['flash_level'=>'success','flash_message'=>'Đã Thêm thành công 1 cột dữ liệu']);
    }

}
