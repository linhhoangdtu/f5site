<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ThanhtoanRequest;
use DB;
use Cart;
use App\Taikhoan;
use User;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getTrangsanpham(){
        $trangsanpham = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia','mota','id_dmsp','id_dm')->orderBy('id','DESC')->get();
        return view('web_thoitrang.quanly.trangsanpham',compact('trangsanpham'));
    }
    public function getDanhmucsanpham($tendm,$id){
        $htdanhmuc = DB::table('danhmuc')->select('id','tendm')->where('id',$id)->get();
    	$danhmucsanpham = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia','dongiakm','id_dmsp','id_dm')->where('id_dm',$id)->get();
        return view('web_thoitrang.quanly.danhmucsanpham',compact('danhmucsanpham','htdanhmuc'));
    }
    public function getSanpham($tendmsp,$id){
        $htdanhmucsp = DB::table('danhmucsanpham')->select('id','tendmsp','id_dm')->where('id',$id)->get();
    	$sanpham = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dmsp','id_dm')->where('id_dmsp',$id)->orderBy('id','DESC')->get();
        $sanphamkm = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dmsp','id_dm')->where('id_dmsp',$id)->orwhere('dongiakm','>',0)->orderBy('id','DESC')->get();
        return view('web_thoitrang.quanly.sanpham',compact('sanpham','sanphamkm','htdanhmucsp'));
    }
    public function getChitietsanpham($tensp,$id){
        $chitiet = DB::table('sanpham')->select('id','tensp','hinhanh','dongia','soluong','mota','dongiakm')->where('id',$id)->get();
    	return view('web_thoitrang.quanly.chitietsanpham',compact('chitiet'));
    }
    public function getMuahang($tensp,$id){
        $muasp =DB::table('sanpham')->where('id',$id)->first();
        $image = $muasp->hinhanh;
        Cart::add(array('id'=>$id,'name'=>$muasp->tensp,'qty'=>1,'price'=>$muasp->dongia,'options'=>array('img'=>$image)));
        $content = Cart::content();
        return redirect()->route('getGiohang');
    }
    public function getGiohang(){
        $giohang = Cart::content();
        $total = Cart::total();
        return view('web_thoitrang.quanly.giohang',compact('giohang','total'));
    }
    public function xoasanpham($id){
        Cart::remove($id);
        return redirect()->route('getGiohang');
    }
    public function capnhatsanpham(){
        if(Request::ajax()){
            $id = Request::get('id');
            $qty = Request::get('qty');
            Cart::update($id,$qty);
            echo "oke";
        }
    }
    public function getSearch(Request $request){
        $name = $request->input('search');
        if($name == '') {
            return redirect()->back();
        } else {
            $sanpham_search = DB::table('sanpham')->where('tensp', 'like', '%'.$name.'%')->get();
            $sp_count = DB::table('sanpham')->where('tensp', 'like', '%'.$name.'%')->count();

            return view('web_thoitrang.pages.search',compact('sanpham_search','sp_count'));
        }
    }
    public function getThanhtoan(){
        $thanhtoan = Cart::content();
        $total = Cart::total();
        $count = Cart::count();
        return view('web_thoitrang.quanly.thanhtoan',compact('thanhtoan','total','count'));
    }
    public function postThanhtoan(ThanhtoanRequest $Request){
        $taikhoan = new Taikhoan;
        $taikhoan->tentk = $Request->txtHoten;
        $taikhoan->email = $Request->txtEmail;
        $taikhoan->sodienthoai = $Request->txtSodienthoai;
        $taikhoan->diachi = $Request->txtDiachi;
        $taikhoan->ghichu = $Request->txtGhichu;
        $taikhoan->save();
        return redirect('/');
    }
}
