<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SanphamRequest;
use App\Sanpham;
use App\Taikhoan;
use App\Danhmuc;
use App\Danhmucsanpham;
use Validator;
class SanphamController extends Controller
{
	public function getList(){
       
		$datasp = Sanpham::select('id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dmsp','id_dm')->orderBy('id','DESC')->get()->toArray();
		return view('admin.sanpham.list',compact('datasp'));
	}
    public function getListdonhang(){
       
        $datasp = Taikhoan::select('id','tentk','email','sodienthoai','diachi','ghichu')->orderBy('id','DESC')->get()->toArray();
        return view('admin.sanpham.listdonhang',compact('datasp'));
    }
    public function getDeletedonhang ($id){
        $taikhoan = Taikhoan::find($id);
        $taikhoan->delete();
        return redirect()->route('admin.sanpham.listdonhang')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
    public function getAdd (){
         $cate = Danhmuc::select('id','tendm')->get()->toArray();
         $catesp = Danhmucsanpham::select('id','tendmsp')->get()->toArray();
    	return view('admin.sanpham.add',compact('cate','catesp'));
    }
    public function postAdd (Request $Requestsp)
    {
        
        $rules = [
            'txtTensp' =>'required|unique:sanpham,tensp',
            'fImages' =>'required:sanpham,hinhanh',
            'txtId_dm' =>'required:sanpham,id_dm',
            'txtId_dmsp' =>'required:sanpham,id_dmsp',
            'txtSoluong' =>'required:sanpham,soluong',
            'txtDongia' =>'required:sanpham,dongia',
            'txtMota' =>'required:sanpham,mota',
            'txtDongiakm' =>'required:sanpham,dongiakm',
        ];

        $messages = [
            'txtTensp.required' => 'please Enter Ten sp',
            'txtTensp.unique' => 'this ten san pham is exit',
            'txtId_dm.required' => 'please Enter Ten danh muc',
            'txtId_dmsp.required' => 'please Enter Ten danh muc san pham',
            'fImages.required' => 'please Enter hinh anh',
            'txtSoluong.required' => 'please Enter so luong',
            'txtDongia.required' => 'please Enter don gia',
            'txtMota.required' => 'please Enter mo ta',
            'txtDongiakm.required' => 'please Enter don gia khuyen mai',
        ];

        $validator = Validator::make($Requestsp->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $file_name = $Requestsp->file('fImages')->getClientOriginalName();
        $sanpham = new Sanpham;
        $sanpham->tensp = $Requestsp->txtTensp;
        $sanpham->hinhanh =$file_name;
        $sanpham->soluong = $Requestsp->txtSoluong;
        $sanpham->dongia = $Requestsp->txtDongia;
        $sanpham->mota = $Requestsp->txtMota;
        $sanpham->dongiakm = $Requestsp->txtDongiakm;
        $sanpham->id_dm = $Requestsp->txtId_dm;
        $sanpham->id_dmsp = $Requestsp->txtId_dmsp;
        $Requestsp->file('fImages')->move('resources/upload',$file_name);
        $sanpham->save();
        return redirect()->route('admin.sanpham.list')->with(['flash_level'=>'success','flash_message'=>'Đã Thêm thành công 1 cột dữ liệu']);
        $sanpham_id = $sanpham->id;
        if(Input::file('fSanphamDetail')){
            foreach(Input::file('fSanphamDetail') as $file){
                $hinhanhsanpham = new hinhanhsanpham();
                if(isset($file)){
                    $hinhanhsanpham->hinhanh = $file->getClientOriginalName();
                    $hinhanhsanpham->sanpham_id = $sanpham_id;
                    $file->move('resources/upload/detail',$file->getClientOriginalName());
                    $hinhanhsanpham->save();
                }
            }
        }
    }
    public function getDelete ($id){
        $sanpham = Sanpham::find($id);
        $sanpham->delete();
        return redirect()->route('admin.sanpham.list')->with(['flash_level'=>'success','flash_message'=>'Đã xóa thành công 1 cột dữ liệu']);
    }
    public function getEdit ($id){
          $data = Sanpham::findOrFail($id)->toArray();
    	$datasp = Sanpham::select('id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dmsp','id_dm')->get()->toArray();
      
    	return view('admin.sanpham.edit',compact('datasp','data','id'));
    }
    public function postEdit (EditsanphamRequest $Requestsp,$id){
        $file_name = $Requestsp->file('fImages')->getClientOriginalName();
        $this->validate($Requestsp,
        ["txtTensp"=>"required"],
        ["txtTensp.required"=>"please enter ten san pham"]
        );
    	$sanpham = Sanpham::find($id);
        $sanpham->tensp = $Requestsp->txtTensp;
        $sanpham->hinhanh =$file_name;
        $sanpham->soluong = $Requestsp->txtSoluong;
        $sanpham->dongia = $Requestsp->txtDongia;
        $sanpham->mota = $Requestsp->txtMota;
        $sanpham->dongiakm = $Requestsp->txtDongiakm;
        $sanpham->id_dm = $Requestsp->txtId_dm;
        $sanpham->id_dmsp = $Requestsp->txtId_dmsp;
        $Requestsp->file('fImages')->move('resources/upload',$file_name);
        $sanpham->save();

         return redirect()->route('admin.sanpham.list')->with(['flash_level'=>'success','flash_message'=>'Đã cập nhật thành công 1 cột dữ liệu']);
    }
}
