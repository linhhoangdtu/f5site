<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SanphamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'txtTensp' =>'required|unique:sanpham,tensp',
            'fImages' =>'required:sanpham,hinhanh',
            'txtId_dm' =>'required:sanpham,id_dm',
            'txtId_dmsp' =>'required:sanpham,id_dmsp',
            'txtSoluong' =>'required:sanpham,soluong',
            'txtDongia' =>'required:sanpham,dongia',
            'txtMota' =>'required:sanpham,mota',
            'txtDongiakm' =>'required:sanpham,dongiakm'
            
        ];
    }
    public function messages(){
        return [
       
        'txtTensp.required' => 'please Enter Ten sp',
        'txtTensp.unique' => 'this ten san pham is exit',
        'txtId_dm.required' => 'please Enter Ten danh muc',
        'txtId_dmsp.required' => 'please Enter Ten danh muc san pham',
        'fImages.required' => 'please Enter hinh anh',
        'txtSoluong.required' => 'please Enter so luong',
        'txtDongia.required' => 'please Enter don gia',
        'txtMota.required' => 'please Enter mo ta'
        'txtDongiakm.required' => 'please Enter don gia khuyen mai'
        ];
    }
}
