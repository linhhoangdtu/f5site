<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KhachhangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtTenkh'=>'required|unique:khachhang,tenkh',
            'txtDiachi'=>'required:khachhang,diachi',
            'txtSdt'=>'required:khachhang,sodienthoai',
            'txtPass'=>'required:khachhang,matkhau'
        ];
    }
    public function messages(){
        return[
            'txtTenkh.required'=>'please ten khach hang',
            'txtTenkh.unique'=>'this ten khach hang is exit',
            'txtDiachi.required'=>'please dia chi',
            'txtSdt.required'=>'please so dien thoai',
            'txtPass.required'=>'please mat khau'
        ];
    }
}
