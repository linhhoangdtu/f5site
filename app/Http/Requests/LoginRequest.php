<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Tentk' => 'required',
            'Matkhau' => 'required'
        ];
    }
    public function mesages()
    {
        return[
        'Tentk.required' =>'please ten dang nhap',
        'Matkhau.required' => 'please mạt khau'
        ];
    }
}
