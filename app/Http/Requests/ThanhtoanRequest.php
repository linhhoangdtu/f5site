<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ThanhtoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtHoten' =>'required',
            'txtEmail' =>'required|email',
            'txtSodienthoai' =>'required',
            'txtDiachi' =>'required',
            'txtGhichu' =>'required'
        ];
    }
    public function messages(){
        return [
       
        'txtHoten.required' => 'please Enter họ tên',
        'txtEmail.email' => 'please Enter email',
        'txtSodienthoai.required' => 'please Enter số điện thoại',
        'txtDiachi.required' => 'please Enter địa chỉ',
        'txtGhichu.required' => 'please Enter ghi chú'
        ];
    }
}
