<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtUser' => 'required|unique:users,name',
            'txtPass' => 'required:users,password',
            'txtEmail' =>'required:users,email'
        ];
    }
    public function messages(){
        return[
            'txtUser.required'=>'please enter username',
            'txtUser.unique'=>'user is exit',
            'txtPass.required' =>'pelase enter pass',
            'txtEmail.required'=>'please enter email'
        ];
    }
}
