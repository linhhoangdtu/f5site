<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DanhmucRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtTendm' => 'required|unique:danhmuc,tendm',
        ];
    }
    public function messages(){
        return[
            'txtTendm.required' => 'Please Enter name danh muc',
            'txtTendm.unique' => 'this name danh muc is exit',
            'txtTendm.required' => 'Please Enter name danh muc'
        ];
    }
}
