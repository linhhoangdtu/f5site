<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditsanphamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtId_dm' =>'required:sanpham,id_dm',
            'txtId_dmsp' =>'required:sanpham,id_dmsp',
            'txtTensp' =>'required:sanpham,tensp',
            'txtSoluong' =>'required:sanpham,soluong',
            'txtDongia' =>'required:sanpham,dongia',
            'txtMota' =>'required:sanpham,mota',
            'txtDongiakm' =>'required:sanpham,dongiakm'
        ];
    }
    public function messages()
    {
        return[
        'txtId_dm.required' => 'please Enter ten danh muc ',
        'txtId_dmsp.required' => 'please Enter ten danh muc san pham',
        'txtTensp.required' => 'please Enter Ten sp',
        'txtSoluong.required' => 'please Enter So luong',
        'txtDongia.required' => 'please Enter Don gia',
        'txtMota.required' => 'please Enter Mo ta',
        'txtDongiakm.required' => 'please Enter Don gia khuyen'
        ];
    }
}
