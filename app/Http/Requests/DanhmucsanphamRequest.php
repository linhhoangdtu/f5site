<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DanhmucsanphamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'txtTendmsp' => 'required|unique:danhmucsanpham,tendmsp'
        ];
    }
    public function messages(){
        return[
            'txtTendmsp.required' => 'Please Enter name danh muc san pham',
            'txtTendmsp.unique' => 'this name danh muc is exit'
        ];
    }
}
