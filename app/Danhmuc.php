<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danhmuc extends Model
{
    protected $table = 'danhmuc';
    protected $fillable=['id','tendm'];
    public $timestamps=false;
    public function sanpham(){
    	return $this->hasMany('App\Sanpham');
    }
    public function danhmucsanpham(){
    	return $this->hasMany('App\Danhmucsanpham');
    }
}
