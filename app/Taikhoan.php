<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taikhoan extends Model
{
    protected $table = 'taikhoan';
    protected $fillable =['id','tentk','email','sodienthoai','diachi','ghichu'];
    public $timestamps=false;
}
