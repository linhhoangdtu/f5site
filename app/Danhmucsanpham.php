<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danhmucsanpham extends Model
{
    protected $table = 'danhmucsanpham';
    protected $fillable=['id','tendmsp','id_dm'];
    public $timestamps=false;
    public function danhmuc(){
    	return $this->belongTo('App\Danhmuc');
    }
    public function sanpham(){
    	return $this->hasMany('App\Sanpham');
    }
}
