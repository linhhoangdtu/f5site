<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image_hienthi extends Model
{
   protected $table = 'image_hienthi';
    protected $fillable=['id','hinhanhht'];
    public $timestamps=false;
    public function danhmuc(){
    	return $this->belongTo('App\Danhmuc');
    }
}
