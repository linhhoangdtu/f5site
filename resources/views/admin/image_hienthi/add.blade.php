@extends('admin.master')
@section('controller','Image_hienthi')
@section('action','Add')
@section('content')
 <div id="page-wrapper">
 

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Hinh Anh Hien thi
                        </h1>
                    </div>
                       <form action="{!! route('admin.image_hienthi.getAdd') !!}" method="POST" enctype="multipart/form-data">
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
@include('admin.blocks.error')

                     
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                         <div class="form-group">
                                <label>Loại Danh Mục</label>
                                <select class="form-control" name="txtId_dm">
                                   @foreach ($cate as $item) {
                                   <option value = "{!!$item["id"]!!}">{!!$item["tendm"]!!}</option>";
                                    @endforeach
    }
                                </select>
                            </div>
                            <div class="form-group">
                                <label>hinhanh</label>
                                <input type="file" name="fImages" value="{!! old('fImages') !!}">
                            </div>
                            <button type="submit" class="btn btn-default">Product Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
              
                    </div>
            <!-- /.container-fluid -->
        </div>
        </div>
                  </form>
@endsection
              