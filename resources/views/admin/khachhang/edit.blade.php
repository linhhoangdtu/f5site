@extends('admin.master')
@section('controller','Khachhang')
@section('action','Edit')
@section('content')
<style>
    .hinhanh_current{width: 200px;}
</style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Khách Hàng
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">

@include('admin.blocks.error')
                        <form action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label>Tên Khách Hàng</label>
                                <input class="form-control" name="txtTenkh" placeholder="Please Enter Tên khách hàng" value="{!! old('txtTenkh',isset($data) ? $data['tenkh'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Mật Khẩu</label>
                                <input class="form-control" name="txtPass" placeholder="Please Enter mật khẩu" value="{!! old('txtPass',isset($data) ? $data['matkhau'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Địa Chỉ</label>
                                <input class="form-control" name="txtDiachi" placeholder="Please Enter Địa chỉ" value="{!! old('txtDiachi',isset($data) ? $data['diachi'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Số Điện Thoại</label>
                                <input class="form-control" name="txtSdt" placeholder="Please Enter số điện thoại" value="{!! old('txtSdt',isset($data) ? $data['sodienthoai'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Product Status</label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="1" checked="" type="radio">Visible
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="2" type="radio">Invisible
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Cập Nhật</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
                  </form>
@endsection
              