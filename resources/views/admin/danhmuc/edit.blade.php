@extends('admin.master')
@section('controller','Danhmuc')
@section('action','Edit')
@section('content')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Mục                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
@include('admin.blocks.error')
                        <form action="" method="POST">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                           
                            <div class="form-group">
                                <label>Tên Danh Mục</label>
                                <input class="form-control" name="txtTendm" placeholder="Please Enter Category Name" value="{!! old('txtTendm',isset($data) ? $data['tendm'] : null) !!}" />
                            </div>
                           
                            <button type="submit" class="btn btn-default">cập Nhật</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                        </div>
                    </div>
            <!-- /.container-fluid -->
        </div>
@endsection
              