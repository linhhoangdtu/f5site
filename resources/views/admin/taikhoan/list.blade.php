@extends('admin.master')
@section('controller','Khachhang')
@section('action','List')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Sách Khách Hàng
                        </h1>
                    </div>
                     @include('admin.blocks.error')
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                   
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên Khách Hàng</th>
                                <th>Địa Chỉ</th>
                                <th>Số Điện Thoại</th>
                                <th>Mật Khẩu</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $stt = 0?>
                        @foreach ($datakh as $kh)
                        <?php $stt =$stt + 1?>
                            <tr class="odd gradeX" align="center">
                                <td>{!! $stt !!}</td>
                                <td>{!! $kh["tenkh"] !!}</td>
                                <td>{!! $kh["diachi"] !!}</td>
                                <td>{!! $kh["sodienthoai"] !!}</td>
                                <td>{!! $kh["matkhau"] !!}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc là muốn xóa không')" href="{!! URL::route('admin.khachhang.getDelete',$kh['id']) !!}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! URL::route('admin.khachhang.getEdit',$kh['id']) !!}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
@endsection