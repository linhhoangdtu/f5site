@extends('admin.master')
@section('controller','Sanpham')
@section('action','Add')
@section('content')
 <div id="page-wrapper">
 

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Khách Hàng
                        </h1>
                    </div>
                       <form action="{!! route('admin.khachhang.getAdd') !!}" method="POST" enctype="multipart/form-data">
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
@include('admin.blocks.error')
                         <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label>Tên Khách Hàng</label>
                                <input class="form-control" name="txtTenkh" placeholder="Please Enter Tên khách hàng"/>
                            </div>
                            <div class="form-group">
                                <label>Mật khẩu</label>
                                <input class="form-control" type="password" name="txtPass" placeholder="Please Enter mật khẩu"/>
                            </div>
                            <div class="form-group">
                                <label>Nhập Lại Mật khẩu</label>
                                <input class="form-control" type="password" name="txtRepass" placeholder="Please Enter mật khẩu"/>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input class="form-control" name="txtDiachi" placeholder="Please Enter Địa chỉ"/>
                            </div>
                            <div class="form-group">
                                <label>Số Điện Thoại</label>
                                <input class="form-control" name="txtSdt" placeholder="Please Enter số điện thoại"/>
                            </div>
                            <div class="form-group">
                                <label>Product Status</label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="1" checked="" type="radio">Visible
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="2" type="radio">Invisible
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Product Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
              
                    </div>
            <!-- /.container-fluid -->
        </div>
        </div>
                  </form>
@endsection
              