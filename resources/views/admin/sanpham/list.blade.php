@extends('admin.master')
@section('controller','Sanpham')
@section('action','List')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Sách Sản Phẩm
                        </h1>
                    </div>
                     @include('admin.blocks.error')
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                   
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên Sản Phẩm</th>
                                <th>Hình Ảnh</th>
                                <th>số Lượng</th>
                                <th>Đơn Giá</th>
                                <th>Mô Tả</th>
                                <th>Đơn Giá Khuyến Mãi</th>
                                <th>Loại Sản Phẩm</th>
                                <th>Loại Danh Mục</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $stt = 0?>
                        @foreach ($datasp as $sp)
                        <?php $stt =$stt + 1?>
                            <tr class="odd gradeX" align="center">
                                <td>{!! $stt !!}</td>
                                <td>{!! $sp["tensp"] !!}</td>
                                <td>{!! $sp["hinhanh"] !!}</td>
                                <td>{!! $sp["soluong"] !!}</td>
                                <td>{!! $sp["dongia"] !!}</td>
                                <td>{!! $sp["mota"] !!}</td>
                                <td>{!! $sp["dongiakm"] !!}</td>
                                <td>
                                    <?php $catesp = DB::table('danhmucsanpham')->where('id',$sp["id_dmsp"])->first(); ?>
                                    @if(!empty($catesp->tendmsp))
                                    {!! $catesp->tendmsp !!}
                                    @endif
                                </td>
                                <td>
                                    <?php $cate = DB::table('danhmuc')->where('id',$sp["id_dm"])->first(); ?>
                                    @if(!empty($cate->tendm))
                                    {!! $cate->tendm !!}
                                    @endif
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc là muốn xóa không')" href="{!! URL::route('admin.sanpham.getDelete',$sp['id']) !!}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! URL::route('admin.sanpham.getEdit',$sp['id']) !!}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
@endsection