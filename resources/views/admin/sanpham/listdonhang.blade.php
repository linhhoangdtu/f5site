@extends('admin.master')
@section('controller','Sanpham')
@section('action','Listdonhang')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Sách Sản Phẩm
                        </h1>
                    </div>
                     @include('admin.blocks.error')
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                   
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên Tài khoản</th>
                                <th>Email</th>
                                <th>Số Điện Thoại</th>
                                <th>Địa Chỉ</th>
                                <th>Ghi Chú</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $stt = 0?>
                        @foreach ($datasp as $sp)
                        <?php $stt =$stt + 1?>
                            <tr class="odd gradeX" align="center">
                                <td>{!! $stt !!}</td>
                                <td>{!! $sp["tentk"] !!}</td>
                                <td>{!! $sp["email"] !!}</td>
                                <td>{!! $sp["sodienthoai"] !!}</td>
                                <td>{!! $sp["diachi"] !!}</td>
                                <td>{!! $sp["ghichu"] !!}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc là muốn xóa không')" href="{!! URL::route('admin.sanpham.getDeletedonhang',$sp['id']) !!}"> Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
@endsection