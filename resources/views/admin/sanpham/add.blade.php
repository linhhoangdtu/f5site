@extends('admin.master')
@section('controller','Sanpham')
@section('action','Add')
@section('content')
 <div id="page-wrapper">
 

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sản Phẩm
                        </h1>
                    </div>
                       <form action="{!! route('admin.sanpham.getAdd') !!}" method="POST" enctype="multipart/form-data">
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
@include('admin.blocks.error')

                     
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-group">
                                <label>Loại Danh Mục</label>
                                <select class="form-control" name="txtId_dm">
                                   @foreach ($cate as $item) {
                                   <option value = "{!!$item["id"]!!}">{!!$item["tendm"]!!}</option>";
                                    @endforeach
    }
                                </select>
                            </div>
                         <div class="form-group">
                                <label>Loại sản phẩm</label>
                                <select class="form-control" name="txtId_dmsp">
                                   @foreach ($catesp as $item) {
                                   <option value = "{!!$item["id"]!!}">{!!$item["tendmsp"]!!}</option>";
                                    @endforeach
    }
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên Sản Phẩm</label>
                                <input class="form-control" name="txtTensp" placeholder="Please Enter Tên sản phẩm"/>
                            </div>
                            <div class="form-group">
                                <label>hinhanh</label>
                                <input type="file" name="fImages" value="{!! old('fImages') !!}">
                            </div>
                            <div class="form-group">
                                <label>Số Lượng</label>
                                <input class="form-control" name="txtSoluong" placeholder="Please Enter Số lượng"/>
                            </div>
                            <div class="form-group">
                                <label>Đơn Giá</label>
                                <input class="form-control" name="txtDongia" placeholder="Please Enter Đơn giá"/>
                            </div>
                            <div class="form-group">
                                 <label>Mô Tả</label>
                                <textarea class="form-control" rows="3" name="txtMota">
                                </textarea> 
                                <script type="text/javascript">ckeditor("txtMota")</script>
                            </div>
                            <div class="form-group">
                                <label>Đơn Giá Khuyến Mãi</label>
                                <input class="form-control" name="txtDongiakm" placeholder="Please Enter Đơn giá khuyến mãi"/>
                            </div>
                            <div class="form-group">
                                <label>Product Status</label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="1" checked="" type="radio">Visible
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="2" type="radio">Invisible
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Product Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
              
                    </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
        @for($i=1;$i<=5;$i++)
            <div class="form-group">
                <label>Image san pham{!! $i !!}</label>
                <input type="file" name="fSanphamDetail[]"/>
            </div>
              @endfor
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        </div>
                  </form>
@endsection
              