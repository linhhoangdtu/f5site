@extends('admin.master')
@section('controller','Sanpham')
@section('action','Edit')
@section('content')
<style>
    .hinhanh_current{width: 200px;}
</style>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sản Phẩm
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">

@include('admin.blocks.error')
                        <form action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-group">
                                <label>Loại Danh Mục</label>
                                <input class="form-control" name="txtId_dm" value="{!! old('txtId_dm',isset($data) ? $data['id_dm'] : null) !!}">
                            </div>
                         <div class="form-group">
                                <label>Loại Danh Mục sản phẩm</label>
                                <input class="form-control" name="txtId_dmsp" value="{!! old('txtId_dmsp',isset($data) ? $data['id_dmsp'] : null) !!}">
                            </div>
                            <div class="form-group">
                                <label>Tên Sản Phẩm</label>
                                <input class="form-control" name="txtTensp" placeholder="Please Enter Tên sản phẩm" value="{!! old('txtTensp',isset($data) ? $data['tensp'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>hinhanh</label>
                                 <img src="{!! asset('resources/upload/'.$data['hinhanh']) !!}" class="hinhanh_current" />
                                <input type="file" name="fImages" value="{!! old('fImages') !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Số Lượng</label>
                                <input class="form-control" name="txtSoluong" placeholder="Please Enter Số lượng" value="{!! old('txtSoluong',isset($data) ? $data['soluong'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Đơn Giá</label>
                                <input class="form-control" name="txtDongia" placeholder="Please Enter Đơn giá" value="{!! old('txtDongia',isset($data) ? $data['dongia'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                 <label>Mô Tả</label>
                                <textarea class="form-control" rows="3" name="txtMota">{!! old('txtMota',isset($data) ? $data['mota'] : null) !!}
                                </textarea> 
                                <script type="text/javascript">ckeditor('txtMota')</script>
                            </div>
                            <div class="form-group">
                                <label>Đơn Giá Khuyến Mãi</label>
                                <input class="form-control" name="txtDongiakm" placeholder="Please Enter Đơn giá Khuyến Mãi" value="{!! old('txtDongiakm',isset($data) ? $data['dongiakm'] : null) !!}"/>
                            </div>
                            <div class="form-group">
                                <label>Product Status</label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="1" checked="" type="radio">Visible
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoStatus" value="2" type="radio">Invisible
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Cập Nhật</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
                  </form>
@endsection
              