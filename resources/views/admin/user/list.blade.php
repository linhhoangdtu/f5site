@extends('admin.master')
@section('controller','User')
@section('action','List')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Sách User
                        </h1>
                    </div>
                     @include('admin.blocks.error')
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                   
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>STT</th>
                                <th>Tên User</th>
                                <th>Email</th>
                                 <th>Level</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $stt = 0?>
                        @foreach ($datauser as $user)
                        <?php $stt =$stt + 1?>
                            <tr class="odd gradeX" align="center">
                                <td>{!! $stt !!}</td>
                                <td>{!! $user["name"] !!}</td>
                                <td>{!! $user["email"] !!}</td>
                                <td>{!! $user["level"] !!}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc là muốn xóa không')" href="{!! URL::route('admin.user.getDelete',$user['id']) !!}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! URL::route('admin.user.getEdit',$user['id']) !!}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
@endsection