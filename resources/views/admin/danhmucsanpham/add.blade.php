@extends('admin.master')
@section('controller','Danhmucsanpham')
@section('action','Add')
@section('content')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Mục Sản Phẩm                      </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
   @include('admin.blocks.error')
                        <form action="{!! route('admin.danhmucsanpham.getAdd') !!}" method="POST">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                           <div class="form-group">
                                <label>Loại Danh Mục sản phẩm</label>
                                <select class="form-control" name="txtId_dm">
                                   @foreach ($cate as $item) {
                                   <option value = "{!!$item["id"]!!}">{!!$item["tendm"]!!}</option>";
                                    @endforeach
                                 }
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên Danh Mục Sản Phẩm</label>
                                <input class="form-control" name="txtTendmsp" placeholder="Please Enter Category Name" />
                            </div>
                           
                            <button type="submit" class="btn btn-default">Category Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                        </div>
                    </div>
            <!-- /.container-fluid -->
        </div>
@endsection
              