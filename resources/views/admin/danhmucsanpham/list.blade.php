@extends('admin.master')
@section('controller','Danhmucsanpham')
@section('action','List')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Sách Danh Mục
                        </h1>
                    </div>
                     @include('admin.blocks.error')
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên Danh Mục Sản Phẩm</th>
                                <th>Tên Danh Mục</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($datadmsp as $item)
                            <tr class="odd gradeX" align="center">
                                <td>{!! $item["id"] !!}</td>
                                <td>{!! $item["tendmsp"] !!}</td>
                                <td>
                                    <?php $cate = DB::table('danhmuc')->where('id',$item["id_dm"])->first(); ?>
                                    @if(!empty($cate->tendm))
                                    {!! $cate->tendm !!}
                                    @endif
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc là muốn xóa không')" href="{!! URL::route('admin.danhmucsanpham.getDelete',$item['id']) !!}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{!! URL::route('admin.danhmucsanpham.getEdit',$item['id']) !!}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
@endsection