<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 index">
	<div class="container">
		<aside class="col-lg-3 col-md-4 hidden-sm hidden-xs">
			<div class="product_list block">
	<div class="block-title">
		<h5><a href="collections/all"><i class="fa fa-bars" aria-hidden="true">&nbsp;</i>Danh mục sản phẩm</a></h5>
	</div>
	<div class="block-content">
	<?php
		$hienthidm = DB::table('danhmuc')->select('id','tendm')->get();
	?>
	@foreach($hienthidm as $dm)
		<li class="level0 parent drop-menu "><a href="{!! url('web_thoitrang/quanly/danh-muc-san-pham',[$dm->tendm,$dm->id]) !!}"><i class="fa fa-caret-right" aria-hidden="true"></i><span>{!! $dm->tendm !!}</span><i class="fa fa-angle-right" aria-hidden="true"></i></a>			  
			
			<ul class="level1">
				<?php
					$hienthidmsp = DB::table('danhmucsanpham')->select('id','tendmsp','id_dm')->where('id_dm',$dm->id)->get();
				?>
			@foreach($hienthidmsp as $dmsp)
				<li class="level1"><a href="{!! url('web_thoitrang/quanly/sanpham',[$dmsp->tendmsp,$dmsp->id]) !!}"><span>{!! $dmsp->tendmsp !!}</span></a></li>
				@endforeach
			</ul>
			
		</li>
		@endforeach
		
		<div class="display_dinao">
			<li class="level0 parent "><a href="/giay-di-choi"><i class="fa fa-caret-right" aria-hidden="true"></i><span>Giày đi chơi</span></a></li>
			
			
			
			
			
			
			
			<li class="level0 parent "><a href="/giay-tre-em"><i class="fa fa-caret-right" aria-hidden="true"></i><span>Giày trẻ em</span></a></li>
			
			
			
			
			
			
			
			<li class="level0 parent active"><a href="/"><i class="fa fa-caret-right" aria-hidden="true"></i><span>Giày</span></a></li>
			
			
			<li class="xoadi"><i class="fa fa-caret-up" aria-hidden="true"></i></li>
		</div>
		<script>
			var li_length = $('.block-content li.level0').length;

			$(document).ready(function(){
				if (li_length <=6 ){
					$(".xemthem").hide();
				} else if (li_length >= 7){
					$(".xemthem").show();
				}
				$(".xemthem").click(function(){
					$(".xemthem").hide();
					$(".display_dinao").show();
				});
				$(".xoadi").click(function (){
					$(".display_dinao").hide();
					$(".xemthem").show();
				});
			});
		</script>
	</div>
</div>
			
			
<div class="online_support block">
	<div class="block-title">
		<h5>Hỗ trợ trực tuyến</h5>
	</div>
	<div class="block-content">
		<div class="sp_1">
			<p>Tư vấn bán hàng 1</p>
			<p>Mrs. Dung: <span>(04) 3786 8904</span></p>
		</div>
		<div class="sp_2">
			<p>Tư vấn bán hàng 2</p>
			<p>Mr. Tuấn: <span>(04) 3786 8904</span></p>
		</div>
		<div class="sp_mail">
			<p>Email liên hệ</p>
			<p><a href="mailto:support@bizweb.vn">support@bizweb.vn</a></p>
		</div>
	</div>
</div>


			
<div class="best_product block">
	<div class="block-title">
		<h5>Sản Phẩm Mới Nhất</h5>
		<div class="hot_sale_navigator">
			<a class="btn prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
			<a class="btn next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
		</div>
	</div>
	<div class="block-content">
		<div class="owl_hot_sale" class="owl-carousel owl-theme">
			<div class="item"> 
			<?php
				$hienthisp = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia')->orderBy('id','DESC')->skip(0)->take(5)->get();
			?>
			@foreach($hienthisp as $sp)	
				<div class="item item_pd">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 item-img">
		
		<a href="/giay-the-thao-converse-4"><img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" width="81" height="81" alt="Giày thể thao Converse 4"></a>
		
	</div>
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 item-info">
		<p class="item-name"><a href="/giay-the-thao-converse-4">{!! $sp->tensp !!}</a></p>
		
		<p class="item-price cl_price fs16"><span>{!! number_format($sp->dongia) !!} VND</span></p>
		
	</div>
</div>	
@endforeach
			</div>
			
		</div>
	</div>
</div>


			
			<div class="quangcao block">
				<img src="{!! url('public/web_thoitrang/banner.jpg') !!}" alt="Quảng cáo"/>
			</div>
			
		</aside>

		<article class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
			
			 <h2><a href="san-pham-noi-bat">Sản phẩm Khuyến Mãi </a></h2>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collection_loop grid-items">
			  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                </div>
                <!-- SAN PHAM -->
                <?php
                    $hienthisanpham = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dmsp','id_dm')->where('dongiakm','>',0)->get();
                ?>
                @foreach($hienthisanpham as $sp)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 loop-grid">
                    
<div class="col-item product-loop-grid bd_red">
    
    <div class="sale-label sale-top-right">{!! number_format(100- ($sp->dongiakm*100/$sp->dongia)) !!} %</div>
    <div class="item-inner">
        <div class="product-wrapper">
            <div class="thumb-wrapper loop-img">
                <a href="{!! url('web_thoitrang/quanly/chi-tiet-san-pham',[$sp->tensp,$sp->id]) !!}" class="thumb flip">
                    <span class="face">
                        <img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" title="Giày Converse Madison Mono" alt="Giày Converse Madison Mono">
                    </span>
                    
                    
                    
                    <span class="face back">
                        <img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" title="Giày Converse Madison Mono" alt="Giày Converse Madison Mono">
                    </span>
                    
                </a>
                <div class="view_buy hidden-xs hidden-sm">
                    
                    <div class="actions">
                        <form action="/cart/add" method="post" class="variants" id="product-actions-2815485" enctype="multipart/form-data">
                            
                            <input type="hidden" name="variantId" value="4566585" />
                            <a href="{!! url('mua-hang',[$sp->tensp,$sp->id]) !!}">Mua ngay</a>
                            
                        </form>
                    </div>
                    
                    
                </div>
            </div>
        </div>
        <div class="item-info">
            <div class="info-inner">
                <h3 class="item-title"> <a href="/giay-converse-madison-mono-leather" title="Giày Converse Madison Mono">{!! $sp->tensp !!}</a> </h3>
                <div class="item-content">
                    <div class="item-price">
                        
                        
                        <div class="price-box"> 
                            
                            <p class="special-price"> 
                                <span class="price-label">Giá khuyến mại</span> 
                                <span class="price">{!! $sp->dongia !!}</span> 
                            </p>
                            <p class="old-price"> 
                                <span class="price-label">Giá cũ:</span> 
                                <span class="price" id="old-price">{!! $sp->dongiakm !!}</span> 
                            </p>
                            
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                </div>

                @endforeach


              
            </div>
  <!--  END sanp ham -->

            	<section class="article_ads hidden-xs">
				<img src="{!! url('public/web_thoitrang/images/article_ads_banner_2.jpg') !!}" width="840" alt="Big Shoe">
			</section>
			 <h2><a href="san-pham-noi-bat">Sản phẩm Nỗi Bật </a></h2>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collection_loop grid-items">
			  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                </div>
                <!-- SAN PHAM -->
                <?php
                    $sanpham = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia','mota','dongiakm','id_dmsp','id_dm')->where('dongia','>',500000)->skip(0)->take(14)->get();
                ?>
                @foreach($sanpham as $sp)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 loop-grid">
                    
<div class="col-item product-loop-grid bd_red">
    
    
    <div class="item-inner">
        <div class="product-wrapper">
            <div class="thumb-wrapper loop-img">
                <a href="{!! url('web_thoitrang/quanly/chi-tiet-san-pham',[$sp->tensp,$sp->id]) !!}" class="thumb flip">
                    <span class="face">
                        <img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" title="Giày Converse Madison Mono" alt="Giày Converse Madison Mono">
                    </span>
                    
                    
                    
                    <span class="face back">
                        <img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" title="Giày Converse Madison Mono" alt="Giày Converse Madison Mono">
                    </span>
                    
                </a>
                <div class="view_buy hidden-xs hidden-sm">
                    
                    <div class="actions">
                        <form action="/cart/add" method="post" class="variants" id="product-actions-2815485" enctype="multipart/form-data">
                            
                            <input type="hidden" name="variantId" value="4566585" />
                            <button class="btn-buy btn-cus add_to_cart" title="Mua ngay"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua ngay</span></button>
                            
                        </form>
                    </div>
                    
                    
                </div>
            </div>
        </div>
        <div class="item-info">
            <div class="info-inner">
                <h3 class="item-title"> <a href="{!! url('web_thoitrang/quanly/chi-tiet-san-pham',[$sp->tensp,$sp->id]) !!}" title="Giày Converse Madison Mono">{!! $sp->tensp !!}</a> </h3>
                <div class="item-content">
                    <div class="item-price">
                        
                        
                        <div class="price-box"> 
                            
                            <p class="special-price"> 
                                <span class="price-label">Giá khuyến mại</span> 
                                <span class="price">{!! $sp->dongia !!}</span> 
                            </p>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                </div>

                @endforeach


                <!--  END sanp ham -->
            </div>
				</div>
			
		</article>

	</div>
</div>
	