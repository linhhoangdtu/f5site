<footer class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="container">
		<div class="row">
			
			<section class="footer-up hidden-xs">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Thông tin công ty</h5>
					
					<li><a href="/huo-ng-da-n" title="Thông tin">Thông tin</a></li>
					
					<li><a href="/gioi-thieu" title="Giới thiệu công ty">Giới thiệu công ty</a></li>
					
					<li><a href="/" title="Hệ thống các siêu thị">Hệ thống các siêu thị</a></li>
					
					<li><a href="/" title="Tuyển dụng">Tuyển dụng</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Hỗ trợ khách hàng</h5>
					
					<li><a href="/huo-ng-da-n" title="Liên hệ và góp ý">Liên hệ và góp ý</a></li>
					
					<li><a href="/huo-ng-da-n" title="Hướng dẫn mua hàng online">Hướng dẫn mua hàng online</a></li>
					
					<li><a href="/huo-ng-da-n" title="Hướng dẫn mua hàng trả góp">Hướng dẫn mua hàng trả góp</a></li>
					
					<li><a href="/die-u-khoa-n-di-ch-vu" title="Quy chế quản lý hoạt động">Quy chế quản lý hoạt động</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Chính sách mua hàng</h5>
					
					<li><a href="/die-u-khoa-n-di-ch-vu" title="Quy định, chính sách">Quy định, chính sách</a></li>
					
					<li><a href="/chi-nh-sa-ch" title="Chính sách bảo hành - Đổi trả">Chính sách bảo hành - Đổi trả</a></li>
					
					<li><a href="/chi-nh-sa-ch" title="Chính sách hội viên">Chính sách hội viên</a></li>
					
					<li><a href="/huo-ng-da-n" title="Giao hàng và lắp đặt">Giao hàng và lắp đặt</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
					<div class="fb-page" data-href="http://www.facebook.com/facebook" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="http://www.facebook.com/facebook">
								<a href="http://www.facebook.com/facebook">Facebook</a>
							</blockquote>
						</div>
					</div>
				</div>
			</section>
			

			<section class="hidden-lg hidden-md hidden-sm col-xs-12 footer-mobile">
				<ul class="topnav">
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Thông tin công ty</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Thông tin</span> </a></li>
							
							
							
							<li class="level1"> <a href="/gioi-thieu"> <span>Giới thiệu công ty</span> </a></li>
							
							
							
							<li class="level1"> <a href="/"> <span>Hệ thống các siêu thị</span> </a></li>
							
							
							
							<li class="level1"> <a href="/"> <span>Tuyển dụng</span> </a></li>
							
							
						</ul>
					</li>
					
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Hỗ trợ khách hàng</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Liên hệ và góp ý</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Hướng dẫn mua hàng online</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Hướng dẫn mua hàng trả góp</span> </a></li>
							
							
							
							<li class="level1"> <a href="/die-u-khoa-n-di-ch-vu"> <span>Quy chế quản lý hoạt động</span> </a></li>
							
							
						</ul>
					</li>
					
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Chính sách mua hàng</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/die-u-khoa-n-di-ch-vu"> <span>Quy định, chính sách</span> </a></li>
							
							
							
							<li class="level1"> <a href="/chi-nh-sa-ch"> <span>Chính sách bảo hành - Đổi trả</span> </a></li>
							
							
							
							<li class="level1"> <a href="/chi-nh-sa-ch"> <span>Chính sách hội viên</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Giao hàng và lắp đặt</span> </a></li>
							
							
						</ul>
					</li>
					
					
				</ul>
				<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
					<div class="fb-page" data-href="http://www.facebook.com/facebook" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="http://www.facebook.com/facebook">
								<a href="http://www.facebook.com/facebook">Facebook</a>
							</blockquote>
						</div>
					</div>
				</div>
			</section>

			<section class="footer-mid">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-mid-1">
					<h5>Hỗ trợ thanh toán</h5>
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-1.jpg?1478571297513" height="40" width="65" alt="Master Card" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-2.jpg?1478571297513" height="40" width="65" alt="Visa" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-3.jpg?1478571297513" height="40" width="65" alt="Tiền mặt" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-4.jpg?1478571297513" height="40" width="65" alt="Chuyển khoản" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-5.jpg?1478571297513" height="40" width="65" alt="ATM nội địa">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-mid-2">
					<h5>Giải đáp thắc mắc</h5>
					<div class="tuvan">
						<img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/tuvan.png?1478571297513" alt="Tư vấn"/>
					</div>
					<div class="f-mid-2-1">
						<p>Tư vấn miễn phí (24/7)</p>
						<p>1900 6750</p>
					</div>
					<div class="f-mid-2-2">
						<p>Góp ý, phản ánh (8h00 - 20h00)</p>
						<p>1900  6750</p>
					</div>
				</div>
			</section>
			<section class="footer-low">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-low-1">
					<h5>Công ty cổ phần Công nghệ DKT</h5>
					<p>Trụ sở chính: Tầng 4 - Tòa nhà Hanoi Group - 442 Đội Cấn - Ba Đình - Hà Nội</p>
					<p>Điện thoại: HN - (04) 6674 2332 - (04) 3786 8904</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-low-2">
					<p>VPDD: Lầu 3 - Tòa nhà Lữ Gia - Số 70 Lữ Gia - P.15 - Q.11 - TP HCM</p>
					<p>HCM - (08) 6680 9686 - (08) 3866 6276</p>
				</div>
			</section>
		</div>
	</div>
</footer>