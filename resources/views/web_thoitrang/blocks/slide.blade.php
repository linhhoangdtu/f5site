<div class="hidden-lg hidden-md hidden-sm col-xs-12 bar_mobile">
	<div class="search_form col-xs-10">
		<form action="/search" method="get" class="search-form" role="search">
			<input placeholder="Nhập từ khóa cần tìm" class="search_input" type="text" name="query" value="" />
			<input type="submit" value="&nbsp" class="btnsearch" />
		</form>
	</div>
	<div class="col-xs-2">
		<a href="tel: 1900 6750">
			<img src="//bizweb.dktcdn.net/thumb/thumb/100/091/132/themes/195294/assets/call.png?1478571297513" width="30" alt="Hotline"/>
		</a>
	</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 slideshow">
	<div class="home_slide">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 slide">
				<div id="owl-slide" class="owl-carousel">
					<div class="item"><img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/slide-img.jpg?1478571297513" alt="Slide"></div>
					<div class="item"><img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/slide-img2.jpg?1478571297513" alt="Slide"></div>
					<div class="item"><img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/slide-img3.jpg?1478571297513" alt="Slide"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$("#owl-slide").owlCarousel({
		slideSpeed : 300,
		paginationSpeed : 400,
		pagination: false,
		itemsCustom : [
			[0, 1],
			[450, 1],
			[600, 1],
			[700, 1],
			[1000, 1],
			[1200, 1],
			[1400, 1],
			[1600, 1]
		],
		autoPlay: true
	});
});
</script>
</div>