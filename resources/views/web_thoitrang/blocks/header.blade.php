﻿<div class="header-container">
		<div class="header-top hidden-sm hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-xs-7">           
						<div class="welcome-msg">Chào mừng bạn đã đến với Lê Quang Khánh! </div>
					</div>
					<div class="social-sharing pull-right">
						<div onclick="location.href='https://facebook.com/congtyDKT'"><i class="fa fa-facebook" aria-hidden="true"></i></div>
						<div onclick="location.href='#'"><i class="fa fa-twitter" aria-hidden="true"></i></div>
						<div onclick="location.href='#'"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
						<div onclick="location.href='#'"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
					</div>
				</div>
			</div>
		</div>

		<div class="container header_main hidden-xs">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-8"> 
					<div class="logo">
						<a title="Big Shoe" href="/">
							
							<img alt="Big Shoe" src="{!! url('public/web_thoitrang/images/logo.png') !!}">
							
						</a> 
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 search">
					<div class="search_vector hidden-sm hidden-xs">
						<ul>
							<h5>Xu hướng tìm kiếm</h5>
							
							<li><a href="/giay-converse">Converse 2016</a></li>
							
							<li><a href="/giay-da">Giày da</a></li>
							
							<li><a href="/giay-thoi-trang-nam">Giày thời trang nam</a></li>
							
						</ul>
					</div>
					<div class="search_form">
						<form action="{!! url('search') !!}" method="get" class="search-form" role="search">
							<input placeholder="Nhập từ khóa cần tìm" class="search_input" type="text" name="search" value="" />
							<input type="submit" value="Tìm kiếm" class="btnsearch" />
						</form>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 hidden-xs account-cart">
					<div class="col-lg-6 col-md-6 col-sm-8 col-xs-6 account">
						<div>
							<img class="mg_bt_10" src="{!! url('public/web_thoitrang/images/account.png') !!}" height="31" width="31" alt="Account"/>
						</div>
						<div>
							<!-- Authentication Links -->
                        @if(Auth::check())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                @if(Auth::user()->level==1)
                                <li><a href="{!! url('admin/danhmuc/add') !!}">Quản Lý</a></li>
                                @endif
                                <li><a href="{!! url('thong-tin') !!}">Thông Tin</a></li>
                                <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                               @else
                            <li><a href="{{ url('login') }}">Login</a></li>
                            <li><a href="{{ url('register') }}">Register</a></li>
                        @endif
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-4 col-xs-6 cart">
						<div class="top-cart-contain"> 
										<div>
											<img href="{!! url('gio-hang') !!}" class="mg_bt_10" src="{!! url('public/web_thoitrang/images/cart.png') !!}" height="31" width="31" alt="cart" />
										</div>
										<div class="cart-box">
											<a href="{!! url('gio-hang') !!}">Giỏ hàng</a>
										</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container hidden-lg hidden-md hidden-sm mobile_menu">
			<div class="row">
				<div class="logo">
					<a title="Big Shoe" href="/">
						
						<img alt="Big Shoe" src="{!! url('public/web_thoitrang/images/logo.png') !!} ">
						
					</a> 
				</div>
				<div class="col-xs-2" id="mobile-menu">
					<ul class="navmenu">
						<li>
							<div class="menutop">
								<div class="toggle">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</div>
							</div>

							<div class="top-cart-contain"> 
								<a href="/cart">
									<div>
										<img src="{!! url('public/web_thoitrang/images/cart.png') !!}" width="32" alt="cart"/>
									</div>
									<div class="cart-box">
										<span id="cart-total">0</span>
									</div>
								</a>
							</div>
							<ul class="submenu">
								<li>
									<ul class="topnav">
										
										
										<li class="level0 level-top parent"> <a class="level-top" href="/"> <span>Trang chủ</span> </a> </li>
										
										
										
										<li class="level0 level-top parent"> <a class="level-top" href="/gioi-thieu"> <span>Về chúng tôi</span> </a> </li>
										
										
										
										<li class="level0 level-top parent"><a class="level-top" href="/collections/all"> <span>Danh mục sản phẩm</span> </a>
											<ul class="level0">
												<?php
													$hienthidm = DB::table('danhmuc')->select('id','tendm')->get();
												?>
												@foreach($hienthidm as $dm)
												<li class="level1 parent"> <a href="/giay-thoi-trang-nam"> <span>{!! $dm->tendm !!}</span> </a>
													<ul class="level1">
														
														
														
													</ul>
												</li>
												@endforeach
												
											</ul>
										</li>
										
										
										
										<li class="level0 level-top parent"> <a class="level-top" href="/tin-tuc"> <span>Tin tức - Blog</span> </a> </li>
										
										
										
										<li class="level0 level-top parent"> <a class="level-top" href="/ban-do"> <span>Bản đồ</span> </a> </li>
										
										
										
										<li class="level0 level-top parent"> <a class="level-top" href="/lien-he"> <span>Liên hệ</span> </a> </li>
										
										
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>