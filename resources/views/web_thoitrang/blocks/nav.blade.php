
	<div class="border-ftw">
		<div class="container">
			<div class="row nav_menu">
				<div class="nav-inner">
					<ul id="nav" class="hidden-xs">
						
						
						<li class="level0 parent active"><a href="{!! url('/') !!}"><span>Trang chủ</span></a></li>
						
						
						
						<li class="level0 parent "><a href="/gioi-thieu"><span>Về chúng tôi</span></a></li>
						
						
									
						<li class="level0 parent drop-menu ">
							<a href="/collections/all"><span>Danh mục sản phẩm</span></a>
							<ul class="level1">
								<?php
									$hienthidm = DB::table('danhmuc')->select('id','tendm')->get();
								?>
								@foreach($hienthidm as $dm)
								<li class="level1 parent"><a href="{!! url('web_thoitrang/quanly/danh-muc-san-pham',[$dm->tendm,$dm->id]) !!}"> <span>{!! $dm->tendm !!}</span> </a><span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
									<ul class="level2 right-sub">
									<?php
										$hienthidmsp = DB::table('danhmucsanpham')->select('id','tendmsp','id_dm')->where('id_dm',$dm->id)->get();
									?>
								@foreach($hienthidmsp as $dmsp)
										<li class="level2"><a href="{!! url('web_thoitrang/quanly/sanpham',[$dmsp->tendmsp,$dmsp->id]) !!}"><span>{!! $dmsp->tendmsp !!}</span></a></li>
										@endforeach
									</ul> 
								</li>
								
								@endforeach
							</ul>
						</li>
						
						
						
						<li class="level0 parent "><a href="/tin-tuc"><span>Tin tức - Blog</span></a></li>
						
						
						
						<li class="level0 parent "><a href="/ban-do"><span>Bản đồ</span></a></li>
						
						
						
						<li class="level0 parent "><a href="/lien-he"><span>Liên hệ</span></a></li>
						
						
					</ul>
					<div class="nav_hotline pull-right hidden-xs hidden-sm">
						<img src="//bizweb.dktcdn.net/thumb/thumb/100/091/132/themes/195294/assets/call_white.png?1478571297513" width="30" alt="Hotline"/> Hotline : 1900 6750
					</div>
				</div>
			</div>
		</div> 
	</div>