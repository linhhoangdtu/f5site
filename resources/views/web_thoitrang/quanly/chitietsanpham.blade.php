
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="vi"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="vi"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="vi"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>
		Website Bán Thời Trang 
	</title>
	
	<meta name="description" content="Big Shoe - Thế giới gi&#224;y của bạn">
	
	<meta name="keywords" content="dkt, bizweb, theme, BigShoe theme"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="https://big-shoe.bizwebvietnam.net"/>
	<meta name='revisit-after' content='1 days' />
	<meta name="robots" content="noodp,index,follow" />
	<meta http-equiv="content-language" content="vi" />
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="shortcut icon" href="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/favicon.png?1478571297513" type="image/x-icon" />
	<script src="{!! url('public/web_thoitrang/js/jquery_1.9.1.min.js') !!}" type='text/javascript'></script> 
<!--<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jquery-ui.js?1478571297513' type='text/javascript'></script>-->
<script src="{!! url('public/web_thoitrang/js/jssocials.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/owl.carousel.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/bootstrap.min.js') !!}" type='text/javascript'></script> 
  
	
<link href="{!! url('public/web_thoitrang/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/blogmate.css') !!}" rel='stylesheet' type='text/css' />
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/flexslider.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/owl.carousel.css') !!}" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="{!! url('public/web_thoitrang/css/font-awesome.min.css') !!}">
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome.min.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/jgrowl.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/style.css') !!}" rel='stylesheet' type='text/css' />
	<script>
var Bizweb = Bizweb || {};
Bizweb.store = 'big-shoe.bizwebvietnam.net';
Bizweb.theme = {"id":195294,"name":"BigShoe 0211","role":"main","previewable":true,"processing":false,"created_on":"2016-11-02T10:46:25Z","modified_on":"2016-11-02T10:46:59Z"}
Bizweb.template = 'index';
</script>

                <script>
                //<![CDATA[
                      (function() {
                        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
                        s.src = '//bizweb.dktcdn.net/assets/themes_support/bizweb_stats.js?v=8';
                        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
                      })();

                //]]>
                </script>
<noscript><iframe height='0' width='0' style='display:none;visibility:hidden' src='/visit/record.gif?p=%2f&r=https%3a%2f%2fthemes.bizweb.vn%2fdemo%2fbig-shoe&s=hrqnsaovqqdrw314rgwr55u1'></iframe></noscript>

<script>
(function() {
function asyncLoad() {
for (var i = 0; i < urls.length; i++) {
var s = document.createElement('script');
s.type = 'text/javascript';
s.async = true;
s.src = urls[i];
s.src = urls[i];
var x = document.getElementsByTagName('script')[0];
x.parentNode.insertBefore(s, x);
}
}
window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
})();
</script>

<script type='text/javascript'>
(function() {
var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
log.src = '//stats.bizweb.vn/delivery/91132.js?lang=vi';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
})();
</script>

<!-- Google Tag Manager -->
<noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
</noscript>
<script>
(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
</script>
<!-- End Google Tag Manager -->



	<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
	<!--[if IE 7]>
<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome-ie7.css?1478571297513' rel='stylesheet' type='text/css' />
<![endif]-->
	<!--[if lt IE 9]>
<script src='//html5shiv.googlecode.com/svn/trunk/html5.js' type='text/javascript'></script>
<![endif]-->

	
	<link href="{!! url('public/web_thoitrang/css/cf-stylesheet.css') !!}" rel='stylesheet' type='text/css' />
	<script>var ProductReviewsAppUtil=ProductReviewsAppUtil || {};</script>
</head>
<body id="giay-converse-camo-americana" class="  cms-index-index cms-home-page" >  
	<header>
	@include('web_thoitrang.blocks.header')
	</div>
</header>

<nav class="hidden-xs">
@include('web_thoitrang.blocks.nav')
</nav>  
<div itemscope itemtype="http://schema.org/Product">
	<meta itemprop="url" content="#">
	<meta itemprop="image" content="{!! url('public/web_thoitrang/images/14-min-1a6a37f9-ca26-409e-9a85-025cc612fd20.jpg') !!}">
	<meta itemprop="shop-currency" content="">
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="col-main">
			@foreach($chitiet as $sp)
				<div class="row product_info">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prd_view_slide">
					<div class="product_title">
								<img src="{!! asset('resources/upload/'.$sp->hinhanh )!!}" alt="{!! $sp->tensp !!}" height="300" width="350">
							</div>
					</div>

					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<div class="product_infomation">
							<div class="product_title">
								<h3 class="clc5 fw_600">{!! $sp->tensp !!}</h3>
							</div>
							<div class="product_price">
								<div class="price-block">
									<div class="price-box">
										
										<p class="special-price"> <span class="price-label">Giá gốc</span> <span class="price prd_price">{!! number_format($sp->dongia) !!} VND</span> </p>
										
										
									</div>
								</div>
							</div>
							
							<div class="product_content hidden-xs">
								<h5 class="fw_600">Mô tả :</h5>
								<div class="cl_old"><p>{!! $sp->mota !!}</p></div>
							</div>
							
						</div>
						<div class="product_pre_buy">
							<div class="add-to-box">
								<div class="add-to-cart">
									
									<form action="{!! url('mua-hang') !!}" method="post" enctype="multipart/form-data" id="add-to-cart-form">
										<div>
													<button class="btn_muangay_list btn_item_loop_list add_to_cart" title="Mua ngay" action="/cart/add"><a href="{!! url('mua-hang',[$sp->tensp,$sp->id]) !!}">Mua ngay</a></button>
											<button class="mobile_cart hidden-lg hidden-md hidden-sm"><a href="tel: 123456789" class="btn_muangay"><i class="fa fa-mobile" aria-hidden="true">&nbsp;</i> Gọi điện</a></button>
											
										</div>
									</form>
									
								</div>
							</div>
						</div>
					</div>
					
					<div style="clear:both;"></div>
				</div>
@endforeach
				<div class="row product_description">
					<div class="prd_tabs">
						<ul class="nav nav-tabs pd-nav" role="tablist">
							<li role="presentation" class="active">
								<a href="#pd-thongtin" aria-controls="pd-thongtin" role="tab" data-toggle="tab"><h5>Thông tin sản phẩm</h5></a>
							</li>
							<li role="presentation">
								<a href="#pd-danhgia" aria-controls="pd-danhgia" role="tab" data-toggle="tab"><h5>Khách hàng đánh giá</h5></a>
							</li>
							<li role="presentation">
								<a href="#pd-thetag" aria-controls="pd-thetag" role="tab" data-toggle="tab"><h5>Thẻ tag</h5></a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="pd-thongtin">
								<div class="thongtin_content cl_old">
									<p style="text-align: justify;">Giày thể thao&nbsp;nam đẹp da màu nâu cao cấp, thanh lịch từ thương hiệu Converse®<br />
Chất liệu giày bằng da bò mềm với chi tiết mũi giày cap-toe<br />
Mắt xỏ dây âm với dây cột nylon<br />
Lót trong bằng da thoáng khí tự nhiên<br />
Đệm lót giày bằng da bọc thoải mái và hỗ trợ chân<br />
Đế ngoài băng cao su hấp thụ sốc tốt và bám tốt trên mọi bề mặt</p>

<p style="text-align: center;"><img src="//bizweb.dktcdn.net/thumb/large/100/091/132/files/3-min.jpg?v=1468392337330" /></p>

<p style="text-align: justify;">Được thành lập vào năm 1978, thương hiệu Nine West xuất phát từ địa chỉ ở thành phố New York. Trong 30 năm, Nine West đã phát triển và trở thành người đứng đầu trong lĩnh vực thời trang nổi tiếng thế giới. Ngày nay, giầy - túi xách - trang sức Nine West được yêu mến bởi phụ nữ trên toàn thế giới và được xem như một chuyên gia tư vấn đáng tin cậy trong mọi lĩnh vực thời trang, bao gồm cả thời trang trẻ em.</p>

<p style="text-align: center;"><img src="//bizweb.dktcdn.net/thumb/large/100/091/132/files/19-min.jpg?v=1468392160053" /></p>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="pd-danhgia">
								<div class="danhgia_content cl_old">
									<div id="bizweb-product-reviews" class="bizweb-product-reviews" data-id="2815480">
	
</div>

<style>
	#bizweb-product-reviews .title-bl{
		display: none;
	}
	#bizweb-product-reviews #bizweb-product-reviews-sub .bizweb-product-reviews-summary .bpr-summary .bpr-summary-average{
		display: none;
	}

	#bizweb-product-reviews #bizweb-product-reviews-sub .bizweb-product-reviews-summary{
		padding: 0;
	}
	.bizweb-product-reviews-star{float:left}
	#bizweb-product-reviews #bizweb-product-reviews-sub .bizweb-product-reviews-summary .bpr-summary-caption{float:left}}
</style>

								</div>
							</div>
							

				
			</div>
			
			
		</div>
	</div>
</div>

				</div>
			</div>
		</div>
	</section>
</div>

<script src='//bizweb.dktcdn.net/assets/themes_support/option-selectors.js?2' type='text/javascript'></script>

<script>  
	var selectCallback = function(variant, selector) {

		var addToCart = jQuery('.btn-cart'),
			productPrice = jQuery('.special-price .price'),
			comparePrice = jQuery('.old-price .price');

		if (variant) {
			if (variant.available) {
				// We have a valid product variant, so enable the submit button
				addToCart.text(' THÊM VÀO GIỎ HÀNG').removeClass('disabled').removeAttr('disabled');
				$('.product-shop .in-stock').text('Còn hàng').removeClass('out-stock');
			} else {
				// Variant is sold out, disable the submit button
				addToCart.text(' HẾT HÀNG').addClass('disabled').attr('disabled', 'disabled');
				$('.product-shop .in-stock').text('Hết hàng').addClass('out-stock');
			}

			// Regardless of stock, update the product price
			productPrice.html(Bizweb.formatMoney(variant.price, "233₫"));

			// Also update and show the product's compare price if necessary
			if ( variant.compare_at_price > variant.price ) {

				comparePrice.html(Bizweb.formatMoney(variant.compare_at_price, "33₫")).show();
			} else {
				comparePrice.hide();     
			}       


		} else {
			// The variant doesn't exist. Just a safeguard for errors, but disable the submit button anyway
			addToCart.text('Hết hàng').attr('disabled', 'disabled');
		}
		/*begin variant image*/
		if (variant && variant.image) {  
			var originalImage = jQuery(".large-image img"); 
			var newImage = variant.image;
			var element = originalImage[0];
			Bizweb.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
				jQuery(element).parents('a').attr('href', newImageSizedSrc);
				jQuery(element).attr('src', newImageSizedSrc);
			});
		}
		/*end of variant image*/
	};
	jQuery(function($) {
		
		new Bizweb.OptionSelectors('product-selectors', {
			product: {"id":2815480,"name":"Giày Converse Camo Americana","alias":"giay-converse-camo-americana","vendor":"Converse","type":"Giày vải","price":650000.0000,"price_max":650000.0000,"price_min":650000.0000,"price_varies":false,"compare_at_price_max":800000.0000,"compare_at_price_min":800000.0000,"compare_at_price_varies":false,"template_layout":"product","tags":["converse","Giày vải","Chuck Taylor"],"meta_title":"Giày Converse Camo Americana","meta_description":"Giày thể thao nam đẹp da màu nâu cao cấp, thanh lịch từ thương hiệu Converse® Chất liệu giày bằng da bò mềm với chi tiết mũi giày cap-toe Mắt xỏ dây âm với dây","summary":"\u003cp\u003e- Chất liệu giày bằng da bò mềm với chi tiết mũi giày cap-toe\u003cbr /\u003e\r\n- Mắt xỏ dây âm với dây cột nylon\u003cbr /\u003e\r\n- Lót trong bằng da thoáng khí tự nhiên\u003c/p\u003e","featured_image":{"id":7610432,"product_id":2815480,"position":"4","src":"//bizweb.dktcdn.net/100/091/132/products/14-min-1a6a37f9-ca26-409e-9a85-025cc612fd20.jpg?v=1468199870023","attached_to_variant":false,"variant_ids":[4566544]},"images":[{"id":7610432,"product_id":2815480,"position":"4","src":"//bizweb.dktcdn.net/100/091/132/products/14-min-1a6a37f9-ca26-409e-9a85-025cc612fd20.jpg?v=1468199870023","attached_to_variant":true,"variant_ids":[4566544]},{"id":7610431,"product_id":2815480,"position":"5","src":"//bizweb.dktcdn.net/100/091/132/products/9-min.jpg?v=1468199870023","attached_to_variant":true,"variant_ids":[4566545]},{"id":7610433,"product_id":2815480,"position":"6","src":"//bizweb.dktcdn.net/100/091/132/products/17-min-75db63a4-662e-4d6a-ae7f-89e96f43c87c.jpg?v=1468199834217","attached_to_variant":true,"variant_ids":[4566546]}],"options":["Màu sắc"],"variants":[{"id":4566544,"price":650000.0000,"compare_at_price":800000.0000,"options":["Rêu"],"option1":"Rêu","title":"Rêu","taxable":false,"inventory_management":"","inventory_policy":"deny","inventory_quantity":1,"weight":0.0,"weight_unit":"kg","image":{"id":7610432,"product_id":2815480,"position":"4","src":"//bizweb.dktcdn.net/100/091/132/products/14-min-1a6a37f9-ca26-409e-9a85-025cc612fd20.jpg?v=1468199870023","attached_to_variant":true,"variant_ids":[4566544]},"requires_shipping":true,"selected":false,"url":"/giay-converse-camo-americana?variantid=4566544","available":true},{"id":4566545,"price":650000.0000,"compare_at_price":800000.0000,"options":["Xanh"],"option1":"Xanh","title":"Xanh","taxable":false,"inventory_management":"","inventory_policy":"deny","inventory_quantity":1,"weight":0.0,"weight_unit":"kg","image":{"id":7610431,"product_id":2815480,"position":"5","src":"//bizweb.dktcdn.net/100/091/132/products/9-min.jpg?v=1468199870023","attached_to_variant":true,"variant_ids":[4566545]},"requires_shipping":true,"selected":false,"url":"/giay-converse-camo-americana?variantid=4566545","available":true},{"id":4566546,"price":650000.0000,"compare_at_price":800000.0000,"options":["Trắng"],"option1":"Trắng","title":"Trắng","taxable":false,"inventory_management":"","inventory_policy":"deny","inventory_quantity":1,"weight":0.0,"weight_unit":"kg","image":{"id":7610433,"product_id":2815480,"position":"6","src":"//bizweb.dktcdn.net/100/091/132/products/17-min-75db63a4-662e-4d6a-ae7f-89e96f43c87c.jpg?v=1468199834217","attached_to_variant":true,"variant_ids":[4566546]},"requires_shipping":true,"selected":false,"url":"/giay-converse-camo-americana?variantid=4566546","available":true}],"available":true,"content":"\u003cp style=\u0022text-align: justify;\u0022\u003eGiày thể thao\u0026nbsp;nam đẹp da màu nâu cao cấp, thanh lịch từ thương hiệu Converse®\u003cbr /\u003e\r\nChất liệu giày bằng da bò mềm với chi tiết mũi giày cap-toe\u003cbr /\u003e\r\nMắt xỏ dây âm với dây cột nylon\u003cbr /\u003e\r\nLót trong bằng da thoáng khí tự nhiên\u003cbr /\u003e\r\nĐệm lót giày bằng da bọc thoải mái và hỗ trợ chân\u003cbr /\u003e\r\nĐế ngoài băng cao su hấp thụ sốc tốt và bám tốt trên mọi bề mặt\u003c/p\u003e\r\n\r\n\u003cp style=\u0022text-align: center;\u0022\u003e\u003cimg src=\u0022//bizweb.dktcdn.net/thumb/large/100/091/132/files/3-min.jpg?v=1468392337330\u0022 /\u003e\u003c/p\u003e\r\n\r\n\u003cp style=\u0022text-align: justify;\u0022\u003eĐược thành lập vào năm 1978, thương hiệu Nine West xuất phát từ địa chỉ ở thành phố New York. Trong 30 năm, Nine West đã phát triển và trở thành người đứng đầu trong lĩnh vực thời trang nổi tiếng thế giới. Ngày nay, giầy - túi xách - trang sức Nine West được yêu mến bởi phụ nữ trên toàn thế giới và được xem như một chuyên gia tư vấn đáng tin cậy trong mọi lĩnh vực thời trang, bao gồm cả thời trang trẻ em.\u003c/p\u003e\r\n\r\n\u003cp style=\u0022text-align: center;\u0022\u003e\u003cimg src=\u0022//bizweb.dktcdn.net/thumb/large/100/091/132/files/19-min.jpg?v=1468392160053\u0022 /\u003e\u003c/p\u003e","summary_or_content":"\u003cp\u003e- Chất liệu giày bằng da bò mềm với chi tiết mũi giày cap-toe\u003cbr /\u003e\r\n- Mắt xỏ dây âm với dây cột nylon\u003cbr /\u003e\r\n- Lót trong bằng da thoáng khí tự nhiên\u003c/p\u003e"},
								   onVariantSelected: selectCallback, 
								   enableHistoryState: true
								   });      

		 

		 // Add label if only one product option and it isn't 'Title'. Could be 'Size'.
		 
		 $('.selector-wrapper:eq(0)').prepend('<label>Màu sắc</label>');
		  

		  // Hide selectors if we only have 1 variant and its title contains 'Default'.
		   
		   $('.selector-wrapper').css({
			   'text-align':'left',
			   'margin-bottom':'15px'
		   });
		   });
</script>
<script>
	function valid(o,w){
		o.value = o.value.replace(valid.r[w],'');
		var qtyCount = document.getElementById('qty').value;
		if(qtyCount == 0)
			document.getElementById("qty").value = 1;
	}
	valid.r={
		'numbers':/[^\d]/g
	}
</script>
	<footer class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="container">
		<div class="row">
			
			<section class="footer-up hidden-xs">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Thông tin công ty</h5>
					
					<li><a href="/huo-ng-da-n" title="Thông tin">Thông tin</a></li>
					
					<li><a href="/gioi-thieu" title="Giới thiệu công ty">Giới thiệu công ty</a></li>
					
					<li><a href="/" title="Hệ thống các siêu thị">Hệ thống các siêu thị</a></li>
					
					<li><a href="/" title="Tuyển dụng">Tuyển dụng</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Hỗ trợ khách hàng</h5>
					
					<li><a href="/huo-ng-da-n" title="Liên hệ và góp ý">Liên hệ và góp ý</a></li>
					
					<li><a href="/huo-ng-da-n" title="Hướng dẫn mua hàng online">Hướng dẫn mua hàng online</a></li>
					
					<li><a href="/huo-ng-da-n" title="Hướng dẫn mua hàng trả góp">Hướng dẫn mua hàng trả góp</a></li>
					
					<li><a href="/die-u-khoa-n-di-ch-vu" title="Quy chế quản lý hoạt động">Quy chế quản lý hoạt động</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Chính sách mua hàng</h5>
					
					<li><a href="/die-u-khoa-n-di-ch-vu" title="Quy định, chính sách">Quy định, chính sách</a></li>
					
					<li><a href="/chi-nh-sa-ch" title="Chính sách bảo hành - Đổi trả">Chính sách bảo hành - Đổi trả</a></li>
					
					<li><a href="/chi-nh-sa-ch" title="Chính sách hội viên">Chính sách hội viên</a></li>
					
					<li><a href="/huo-ng-da-n" title="Giao hàng và lắp đặt">Giao hàng và lắp đặt</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
					<div class="fb-page" data-href="http://www.facebook.com/facebook" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="http://www.facebook.com/facebook">
								<a href="http://www.facebook.com/facebook">Facebook</a>
							</blockquote>
						</div>
					</div>
				</div>
			</section>
			

			<section class="hidden-lg hidden-md hidden-sm col-xs-12 footer-mobile">
				<ul class="topnav">
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Thông tin công ty</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Thông tin</span> </a></li>
							
							
							
							<li class="level1"> <a href="/gioi-thieu"> <span>Giới thiệu công ty</span> </a></li>
							
							
							
							<li class="level1"> <a href="/"> <span>Hệ thống các siêu thị</span> </a></li>
							
							
							
							<li class="level1"> <a href="/"> <span>Tuyển dụng</span> </a></li>
							
							
						</ul>
					</li>
					
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Hỗ trợ khách hàng</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Liên hệ và góp ý</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Hướng dẫn mua hàng online</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Hướng dẫn mua hàng trả góp</span> </a></li>
							
							
							
							<li class="level1"> <a href="/die-u-khoa-n-di-ch-vu"> <span>Quy chế quản lý hoạt động</span> </a></li>
							
							
						</ul>
					</li>
					
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Chính sách mua hàng</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/die-u-khoa-n-di-ch-vu"> <span>Quy định, chính sách</span> </a></li>
							
							
							
							<li class="level1"> <a href="/chi-nh-sa-ch"> <span>Chính sách bảo hành - Đổi trả</span> </a></li>
							
							
							
							<li class="level1"> <a href="/chi-nh-sa-ch"> <span>Chính sách hội viên</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Giao hàng và lắp đặt</span> </a></li>
							
							
						</ul>
					</li>
					
					
				</ul>
				<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
					<div class="fb-page" data-href="http://www.facebook.com/facebook" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="http://www.facebook.com/facebook">
								<a href="http://www.facebook.com/facebook">Facebook</a>
							</blockquote>
						</div>
					</div>
				</div>
			</section>

			<section class="footer-mid">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-mid-1">
					<h5>Hỗ trợ thanh toán</h5>
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-1.jpg?1478571297513" height="40" width="65" alt="Master Card" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-2.jpg?1478571297513" height="40" width="65" alt="Visa" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-3.jpg?1478571297513" height="40" width="65" alt="Tiền mặt" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-4.jpg?1478571297513" height="40" width="65" alt="Chuyển khoản" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-5.jpg?1478571297513" height="40" width="65" alt="ATM nội địa">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-mid-2">
					<h5>Giải đáp thắc mắc</h5>
					<div class="tuvan">
						<img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/tuvan.png?1478571297513" alt="Tư vấn"/>
					</div>
					<div class="f-mid-2-1">
						<p>Tư vấn miễn phí (24/7)</p>
						<p>1900 6750</p>
					</div>
					<div class="f-mid-2-2">
						<p>Góp ý, phản ánh (8h00 - 20h00)</p>
						<p>1900  6750</p>
					</div>
				</div>
			</section>
			<section class="footer-low">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-low-1">
					<h5>Công ty cổ phần Công nghệ DKT</h5>
					<p>Trụ sở chính: Tầng 4 - Tòa nhà Hanoi Group - 442 Đội Cấn - Ba Đình - Hà Nội</p>
					<p>Điện thoại: HN - (04) 6674 2332 - (04) 3786 8904</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-low-2">
					<p>VPDD: Lầu 3 - Tòa nhà Lữ Gia - Số 70 Lữ Gia - P.15 - Q.11 - TP HCM</p>
					<p>HCM - (08) 6680 9686 - (08) 3866 6276</p>
				</div>
			</section>
		</div>
	</div>
</footer>
<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 copyright">
	<div class="container">
		<div class="row mg0">
			<span>© Bản quyền thuộc về Avent Team </span>
			<br class="hidden-lg hidden-md hidden-sm">
			<span class="hidden-xs">&nbsp;|&nbsp;</span>
			<span> Cung cấp bởi <a href="https://www.bizweb.vn/?utm_source=site-khach-hang&utm_campaign=referral_bizweb&utm_medium=footer&utm_content=cung-cap-boi-bizweb" target="_blank" title="Bizweb" style="color:#ec5d5d;">Bizweb</a></span>
		</div>
	</div>
</section>
	<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/common.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jquery.flexslider.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/cloud-zoom.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/owl.carousel.min.js?1478571297513' type='text/javascript'></script> 
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/parallax.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/assets/themes_support/api.jquery.js?2' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jgrowl.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/cs.script.js?1478571297513' type='text/javascript'></script>
	<script type="text/javascript">
	Bizweb.updateCartFromForm = function(cart, cart_summary_id, cart_count_id) {
		if ((typeof cart_summary_id) === 'string') {
			var cart_summary = jQuery(cart_summary_id);
			if (cart_summary.length) {
				// Start from scratch.
				cart_summary.empty();
				// Pull it all out.        
				jQuery.each(cart, function(key, value) {
					if (key === 'items') {
						var table = jQuery(cart_summary_id);           
						if (value.length) {           
							jQuery.each(value, function(i, item) {	
								var link_img1 = Bizweb.resizeImage(item.image, 'small');
								if(
									link_img1 =="null" || link_img1 =='' || link_img1 == null){
									link_img1 = 'https://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif';
								}
								jQuery('<li class="item"><a class="product-image" href="' + item.url + '" title="' + item.name + '"><img alt="'+  item.name  + '" src="' + link_img1 +  '"width="'+ '80' +'"\></a><div class="detail-item"><div class="product-details"> <a href="javascript:void(0);" title="Xóa" onclick="Bizweb.removeItem(' + item.variant_id + ')" class="fa fa-remove">&nbsp;</a><p class="product-name"> <a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></p></div><div class="product-details-bottom"> <span class="price">' + Bizweb.formatMoney(item.price, "33₫") + '</span> <span class="title-desc">Số lượng:</span> <strong>' +  item.quantity + '</strong> </div></div></li>').appendTo(table);
							}); 
							jQuery('<div style="float:left; height:102px;"></div>').appendTo(table);
							jQuery('<li class="cart_fix_1"><div class="top-subtotal">Tổng cộng: <span class="price">' + Bizweb.formatMoney(cart.total_price, "55₫") + '</span></div></li>').appendTo(table);
							jQuery('<li class="cart_fix_2" style="margin-left:-15px;margin-right:-15px;"><div class="actions"><button class="btn-checkout" type="button" onclick="window.location.href=\'/checkout\'"><span>Thanh toán</span></button><button class="view-cart" type="button" onclick="window.location.href=\'/cart\'" ><span>Giỏ hàng</span></button></div></li>').appendTo(table);
						}
						else {
							jQuery('<li class="item"><p>Không có sản phẩm nào trong giỏ hàng.</p></li>').appendTo(table);
						}
					}
				});
			}
		}
		updateCartDesc(cart);
	}
	function updateCartDesc(data){
		var $cartLinkText = $('.mini-cart .cart-box #cart-total, aside.sidebar .block-cart .amount a'),
			$cartPrice = Bizweb.formatMoney(data.total_price, "55₫");		
		switch(data.item_count){
			case 0:
				$cartLinkText.text('0');
				break;
			case 1:
				$cartLinkText.text('1');
				break;
			default:
				$cartLinkText.text(data.item_count);
				break;
		}
		$('.top-cart-content .top-subtotal .price, aside.sidebar .block-cart .subtotal .price').html($cartPrice);
	}  
	Bizweb.onCartUpdate = function(cart) {
		Bizweb.updateCartFromForm(cart, '.top-cart-content .mini-products-list', 'shopping-cart');
	};  
	$(window).load(function() {
		// Let's get the cart and show what's in it in the cart box.  
		Bizweb.getCart(function(cart) {      
			Bizweb.updateCartFromForm(cart, '.top-cart-content .mini-products-list');    
		});
	});
</script>
	<!--<a href="#" id="toTop"><span id="toTopHover"></span></a>-->
	<!--	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5565f9cf142bb517"></script>-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>