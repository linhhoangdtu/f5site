


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Shop name - Thanh toán đơn hàng" />
    
    <link rel="shortcut icon" href="{!! url('public/web_thoitrang/images/favicon.ico') !!}" type="image/x-icon" />
    
     
    <link href= "{!! url('public/web_thoitrang/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' />
    <link href="{!! url('public/web_thoitrang/css/nprogress.css') !!}" rel='stylesheet' type='text/css' />
    <link href="{!! url('public/web_thoitrang/css/font-awesome.min.css') !!}" rel='stylesheet' type='text/css' />
    <link href="{!! url('public/web_thoitrang/css/checkout.css') !!}" rel='stylesheet' type='text/css' />

    
    <link href="{!! url('public/web_thoitrang/css/checkout.css') !!}" rel='stylesheet' type='text/css' />
    

    <title>Trang Website Bán Hàng - Thanh toán đơn hàng</title>
    <script type="text/javascript">
        function addDOMLoadedEvent(func) {
            if (document.readyState === 'complete') {
                func();
            } else {
                if (document.addEventListener) {
                    document.addEventListener("DOMContentLoaded", func, false);
                }
                else {
                    document.attachEvent("onDOMContentLoaded", func);
                }
            }
        }
    </script>
    <script>
    //<![CDATA[
        (function() {
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
            s.src = '//bizweb.dktcdn.net/assets/themes_support/bizweb_stats.js?v=1';
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    //]]>
    </script>
</head>
<body class="body--custom-background-color ">
    
    <form action="{!! route('getThanhtoan') !!}" method="post" enctype="multipart/form-data" data-toggle="validator" class="formCheckout">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<div class="main">
<div class="wrap clearfix">
<div class="row">
<div class="col-md-4 col-sm-12 order-info" define="{order_expand: false}">
<div class="order-summary order-summary--custom-background-color ">
    <div class="order-summary-header summary-header--thin summary-header--border">
        <h2>
            <label class="control-label">Đơn hàng</label>
            <label class="control-label" type="hidden" name="txttotal_soluong" value="{!! Cart::count() !!}">({!! $count !!})</label>
        </h2>
    </div>
    <div class="order-items mobile--is-collapsed" bind-class="{'mobile--is-collapsed': !order_expand}">
        <div class="summary-body summary-section summary-product" >
            <div class="summary-product-list">
                <ul class="product-list">
                     @foreach($thanhtoan as $sp)
                    <li class="product product-has-image clearfix">
                        <div class="product-thumbnail pull-left">
                            <div class="product-thumbnail__wrapper">
                                <img title="{!! $sp->name !!}" src="{!! url('resources/upload/'.$sp['options']['img']) !!}" width="55" height="50" class="product-thumbnail__image">
                                
                            </div>
                            <span class="product-thumbnail__quantity" aria-hidden="true">{!! $sp->qty !!}</span>

                        </div>
                        <div class="product-info pull-left">
                            <span class="product-info-name">
                                <strong>{!! $sp->name !!}</strong>
                            </span>
                        </div>

                        <strong class="product-price pull-right">
                            {!! number_format($sp->price,0,",",".") !!} VND
                        </strong>
                        
                    </li>
                     @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="summary-section border-top-none--mobile">
        <div class="total-line total-line-total clearfix">
            <span class="total-line-name pull-left">
                Tổng cộng
            </span>
            <span bind="money(totalOrderItemPrice + (isNaN(shippingFee) ? 0 : shippingFee) - discount,'55₫')" class="total-line-price pull-right" type="hidden" name="txttotal_dongia" value="{!! Cart::total() !!}">{!! number_format($total,0,",",".") !!} VND
            </span>
        </div>
    </div>
</div>
 <div class="form-group clearfix hidden-sm hidden-xs">
    @if(Cart::count()>0)
    <input class="btn btn-primary col-md-12 mt10 btn-checkout" type="submit" bind-event-click="paymentCheckout()"  value="ĐẶT HÀNG" />
    @endif
</div>
</div>
<div class="col-md-4 col-sm-12 customer-info">
                            <div define="{billing_address: {},billing_expand:true}" class="form-group m0">
                                <h2>
                                    <label class="control-label">Thông tin mua hàng</label>
                                </h2>
                            </div>
                            <hr class="divider">
                            <div class="billing">
                                <div class="form-group">
                                    <a class="underline-none" bind-event-click="billing_expand = !billing_expand" bind-class="{expandable: otherAddress, open: billing_expand}" href="javascript:void(0)">
                                        <span bind-show="!otherAddress">Thông tin thanh toán và nhận hàng</span>
                                        <span bind-show="otherAddress" class="hide">Thông tin thanh toán</span>
                                    </a>
                                </div>
                                <div bind-show="shipping_expand || !otherAddress">
                                    <div class="form-group">
                                        <input class="form-control" type="text" data-error="Vui lòng nhập họ tên" name="txtHoten" value="{!! old('txtHoten') !!}" placeholder="Họ và tên" />
                                        <div class="help-block with-errors"><?php echo $errors->first('txtHoten'); ?></div>
                                    </div>
                                     <div class="form-group" bind-class="{'has-error' : invalidEmail}">
                                <input data-error="Vui lòng nhập email đúng định dạng" value="{!! old('txtEmail') !!}" name="txtEmail" type="email" class="form-control" placeholder="Email" pattern="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"/>
                                <div class="help-block with-errors"><?php echo $errors->first('txtEmail'); ?></div>
                                </div>
                                </div>
                                        <div class="form-group">
                                            <input type="text" name="txtSodienthoai" value="{!! old('txtSodienthoai') !!}" class="form-control"  placeholder="Số điện thoại" />
                                            <div class="help-block with-errors"><?php echo $errors->first('txtSodienthoai'); ?></div>
                                        </div>
                                    
                                    
                                        <div class="form-group">
                                            <input type="text" name="txtDiachi" class="form-control" value="{!! old('txtDiachi') !!}" placeholder="Địa chỉ" />
                                            <div class="help-block with-errors"><?php echo $errors->first('txtDiachi'); ?></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="txtGhichu" value="{!! old('txtGhichu') !!}" class="form-control" placeholder="Ghi chú" />
                                            <div class="help-block with-errors"><?php echo $errors->first('txtGhichu'); ?></div>
                                        </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                            <div class="shipping-method" bind-show="shippingMethods.length > 0">
                                <div class="form-group">
                                    <h2>
                                         <a href="{!! url('/') !!}" style="background-color: blue;padding: 12pt;padding-right: 190pt;">Trang Chủ</a>
                                    </h2>
                                </div>
                            </div>
                            </div>
                        </div>
        </div>
        </div>
        </div>
    </form>

    <script src="{!! url('public/web/js/jquery-2.1.3.min.js') !!}" type='text/javascript'></script>
    <script src="{!! url('public/web/js/bootstrap.min.js') !!}" type='text/javascript'></script>
    <script src="{!! url('public/web/js/twine.min.js') !!}" type='text/javascript'></script>
    <script src="{!! url('public/web/js/validator.min.js') !!}" type='text/javascript'></script>
    <script src="{!! url('public/web/js/nprogress.js') !!}" type='text/javascript'></script>
    <script src="{!! url('public/web/js/money-helper.js') !!}" type='text/javascript'></script>
    <script src="{!! url('public/web/js/checkout.js') !!}" type='text/javascript'></script>

    <script type="text/javascript">
        $(document).ajaxStart(function () {
            NProgress.start();
        });
        $(document).ajaxComplete(function () {
            NProgress.done();
        });

        context = {}

        $(function () {
            Twine.reset(context).bind().refresh();
        });
        
        $(document).ready(function () {
            
            $("#customer-address").trigger("change");
            
            $("select[name='BillingProvinceId']").trigger("change");
            $("select[name='ShippingProvinceId']").trigger("change");
            Twine.context(document.body).checkout.caculateShippingFee();
        });
    </script>
    <script type='text/javascript'>
        
    </script>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
    </noscript>
    <script>
        (function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
        '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
    </script>
    <!-- End Google Tag Manager -->
</body>
</html>