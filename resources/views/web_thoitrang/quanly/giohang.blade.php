
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="vi"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="vi"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="vi"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>
		Website Bán Thời Trang 
	</title>
	
	<meta name="description" content="Big Shoe - Thế giới gi&#224;y của bạn">
	
	<meta name="keywords" content="dkt, bizweb, theme, BigShoe theme"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="https://big-shoe.bizwebvietnam.net"/>
	<meta name='revisit-after' content='1 days' />
	<meta name="robots" content="noodp,index,follow" />
	<meta http-equiv="content-language" content="vi" />
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="shortcut icon" href="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/favicon.png?1478571297513" type="image/x-icon" />
	<script src="{!! url('public/web_thoitrang/js/jquery_1.9.1.min.js') !!}" type='text/javascript'></script> 
<!--<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jquery-ui.js?1478571297513' type='text/javascript'></script>-->
<script src="{!! url('public/web_thoitrang/js/jssocials.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/owl.carousel.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/bootstrap.min.js') !!}" type='text/javascript'></script> 
  
	
<link href="{!! url('public/web_thoitrang/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/blogmate.css') !!}" rel='stylesheet' type='text/css' />
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/flexslider.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/owl.carousel.css') !!}" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="{!! url('public/web_thoitrang/css/font-awesome.min.css') !!}">
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome.min.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/jgrowl.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/style.css') !!}" rel='stylesheet' type='text/css' />
	<script>
var Bizweb = Bizweb || {};
Bizweb.store = 'big-shoe.bizwebvietnam.net';
Bizweb.theme = {"id":195294,"name":"BigShoe 0211","role":"main","previewable":true,"processing":false,"created_on":"2016-11-02T10:46:25Z","modified_on":"2016-11-02T10:46:59Z"}
Bizweb.template = 'cart';
</script>

                <script>
                //<![CDATA[
                      (function() {
                        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
                        s.src = '//bizweb.dktcdn.net/assets/themes_support/bizweb_stats.js?v=8';
                        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
                      })();

                //]]>
                </script>

<script>
(function() {
function asyncLoad() {
var urls = ["//productreviews.bizwebapps.vn/assets/js/productreviews.min.js?store=big-shoe.bizwebvietnam.net","https://collectionfilter.bizwebapps.vn/genscript/script.js?store=big-shoe.bizwebvietnam.net"];
for (var i = 0; i < urls.length; i++) {
var s = document.createElement('script');
s.type = 'text/javascript';
s.async = true;
s.src = urls[i];
s.src = urls[i];
var x = document.getElementsByTagName('script')[0];
x.parentNode.insertBefore(s, x);
}
}
window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
})();
</script>

<script type='text/javascript'>
(function() {
var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
log.src = '//stats.bizweb.vn/delivery/91132.js?lang=vi';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
})();
</script>

<!-- Google Tag Manager -->
<noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
</noscript>
<script>
(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
</script>
<!-- End Google Tag Manager -->



	<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
	<!--[if IE 7]>
<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome-ie7.css?1478571297513' rel='stylesheet' type='text/css' />
<![endif]-->
	<!--[if lt IE 9]>
<script src='//html5shiv.googlecode.com/svn/trunk/html5.js' type='text/javascript'></script>
<![endif]-->

	
	<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/cf-stylesheet.css?1478571297513' rel='stylesheet' type='text/css' />
	<script>var ProductReviewsAppUtil=ProductReviewsAppUtil || {};</script>
</head>
<body id="gio-hang" class="  cms-index-index cms-home-page" >  
	<header>
	@include('web_thoitrang.blocks.header')
</header>

<nav class="hidden-xs">
	@include('web_thoitrang.blocks.nav')
</nav>  
	<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="inner">
				<ul>
					<li class="home"> <a title="Quay lại trang chủ" href="/">Trang chủ</a></li>
					
					<i class="fa fa-angle-double-right" aria-hidden="true"></i>
					<li><span class="brn">Giỏ hàng</span></li>
					
				</ul>
			</div>
		</div>
	</div>
</div>

<section class="main-container col1-layout">
	<div class="main container">
		<div class="col-main cart-page">
			<div class="cart">
				
				<form action="/cart" method="post" novalidate>
					<div class="table-responsive">
						<fieldset>
							<table class="data-table cart-table" id="shopping-cart-table">
								<colgroup>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
								</colgroup>
								<thead>
									<tr class="first last">
										<th rowspan="1">Ảnh sản phẩm</th>
										<th rowspan="1"><span class="nobr">Tên sản phẩm</span></th>
										<th colspan="1" class="a-center">Đơn giá</th>
										<th class="a-center" rowspan="1">Số lượng</th>
										<th colspan="1" class="a-center">Thành tiền</th>
										<th rowspan="1">Xoá</th>
										<th rowspan="1">update</th>
									</tr>
								</thead>

								<tbody>
								@foreach($giohang as $sp)
									
									<tr>
										<td class="image">
											
											<a class="product-image" href="/giay-converse-star-collar-break"  title="{!! $sp->name !!}">
												<img width="150" height="150" alt="{!! $sp->name !!}" src="{!! url('resources/upload/'.$sp['options']['img']) !!}" alt="Giày Converse Star Collar Break">
											</a>
											
										</td>

										<td>
											<h2>
												<a class="product-name" href="/giay-converse-star-collar-break">{!! $sp->name !!}</a>          
											</h2>
											
											
										</td>
										<td>
											<span class="cart-price">
												<span class="cl_price fs18 fw600">{!! $sp->price !!} VND</span>
											</span>
										</td>
										<td class="txt_center">
											<input type="text" maxlength="12" min="0" class="input-text qty" title="Số lượng" size="1" value="{!! $sp['qty'] !!}" name="txtSoluong" id="updates_1295154">
										</td>
										<td>
											<span class="cart-price">
												<span class="cl_price fs18 fw600">{!! number_format($sp->price * $sp->qty) !!} VND</span>
											</span>
										</td>
										<td class="txt_center">                     
											<a class="button remove-item" title="Xóa" href="{!! url('xoa-san-pham',['id'=>$sp['rowid']]) !!}" data-id="1295154"><span><span><img src="{!! url('public/web_thoitrang/images/xoa.png') !!}" /></span></span></a>
										</td>
										<td class="txt_center">                     
											<a href="gio-hang" class="updatecart" title="Cập nhật" data-id="1295154" id="{!! $sp['rowid'] !!}"><img src="{!! url('public/web_thoitrang/images/xoa.png') !!}" /></a>
										</td>
									</tr>
									
									@endforeach
								</tbody>
								<tfoot>
									<tr class="first last">
										<td class="last" colspan="7">
											<button class="btn-continue btn-cart" title="Tiếp tục mua hàng" type="button" >
												<a href="{!! url('/') !!}"> Tiếp tục</a></
											</button>
										</td>
									</tr>
								</tfoot>
							</table>
						</fieldset>
					</div>
				</form>
				
				<div class="cart-collaterals row">        
					  
					<div class="totals col-sm-6 col-md-5 col-xs-12 col-md-offset-7">
						<div class="inner">
							<table class="table shopping-cart-table-total" id="shopping-cart-totals-table">
								<tbody>
									<tr>
										<td>Tổng giá sản phẩm</td>
										<td class="a-right"><strong><span class="cl_price fs18">{!! number_format($total) !!} VND</span></strong></td>
									</tr>
								</tbody>
								
							</table>
							<ul class="checkout">
								<li>
									<button class="btn-proceed-checkout" title="Tiến hành thanh toán" type="button" onclick="window.location.href='{!!url('thanh-toan') !!}'">
										<span>Tiến hành thanh toán</span>
									</button>
								</li>              
							</ul>
						</div>

					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
	@include('web_thoitrang.blocks.footer')
<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 copyright">
	<div class="container">
		<div class="row mg0">
			<span>© Bản quyền thuộc về Avent Team </span>
			<br class="hidden-lg hidden-md hidden-sm">
			<span class="hidden-xs">&nbsp;|&nbsp;</span>
			<span> Cung cấp bởi <a href="https://www.bizweb.vn/?utm_source=site-khach-hang&utm_campaign=referral_bizweb&utm_medium=footer&utm_content=cung-cap-boi-bizweb" target="_blank" title="Bizweb" style="color:#ec5d5d;">Bizweb</a></span>
		</div>
	</div>
</section>
	<script src="{!! url('public/web_thoitrang/js/common.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/jquery.flexslider.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/cloud-zoom.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/owl.carousel.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/parallax.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/api.jquery.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/jgrowl.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/cs.script.js') !!}" type='text/javascript'></script>
<script src="{!! url('public/web_thoitrang/js/myscript.js') !!}" type='text/javascript'></script>
	<script type="text/javascript">
	Bizweb.updateCartFromForm = function(cart, cart_summary_id, cart_count_id) {
		if ((typeof cart_summary_id) === 'string') {
			var cart_summary = jQuery(cart_summary_id);
			if (cart_summary.length) {
				// Start from scratch.
				cart_summary.empty();
				// Pull it all out.        
				jQuery.each(cart, function(key, value) {
					if (key === 'items') {
						var table = jQuery(cart_summary_id);           
						if (value.length) {           
							jQuery.each(value, function(i, item) {	
								var link_img1 = Bizweb.resizeImage(item.image, 'small');
								if(
									link_img1 =="null" || link_img1 =='' || link_img1 == null){
									link_img1 = 'https://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif';
								}
								jQuery('<li class="item"><a class="product-image" href="' + item.url + '" title="' + item.name + '"><img alt="'+  item.name  + '" src="' + link_img1 +  '"width="'+ '80' +'"\></a><div class="detail-item"><div class="product-details"> <a href="javascript:void(0);" title="Xóa" onclick="Bizweb.removeItem(' + item.variant_id + ')" class="fa fa-remove">&nbsp;</a><p class="product-name"> <a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></p></div><div class="product-details-bottom"> <span class="price">' + Bizweb.formatMoney(item.price, "55₫") + '</span> <span class="title-desc">Số lượng:</span> <strong>' +  item.quantity + '</strong> </div></div></li>').appendTo(table);
							}); 
							jQuery('<div style="float:left; height:102px;"></div>').appendTo(table);
							jQuery('<li class="cart_fix_1"><div class="top-subtotal">Tổng cộng: <span class="price">' + Bizweb.formatMoney(cart.total_price, "55₫") + '</span></div></li>').appendTo(table);
							jQuery('<li class="cart_fix_2" style="margin-left:-15px;margin-right:-15px;"><div class="actions"><button class="btn-checkout" type="button" onclick="window.location.href=\'/checkout\'"><span>Thanh toán</span></button><button class="view-cart" type="button" onclick="window.location.href=\'/cart\'" ><span>Giỏ hàng</span></button></div></li>').appendTo(table);
						}
						else {
							jQuery('<li class="item"><p>Không có sản phẩm nào trong giỏ hàng.</p></li>').appendTo(table);
						}
					}
				});
			}
		}
		updateCartDesc(cart);
	}
	function updateCartDesc(data){
		var $cartLinkText = $('.mini-cart .cart-box #cart-total, aside.sidebar .block-cart .amount a'),
			$cartPrice = Bizweb.formatMoney(data.total_price, "55₫");		
		switch(data.item_count){
			case 0:
				$cartLinkText.text('0');
				break;
			case 1:
				$cartLinkText.text('1');
				break;
			default:
				$cartLinkText.text(data.item_count);
				break;
		}
		$('.top-cart-content .top-subtotal .price, aside.sidebar .block-cart .subtotal .price').html($cartPrice);
	}  
	Bizweb.onCartUpdate = function(cart) {
		Bizweb.updateCartFromForm(cart, '.top-cart-content .mini-products-list', 'shopping-cart');
	};  
	$(window).load(function() {
		// Let's get the cart and show what's in it in the cart box.  
		Bizweb.getCart(function(cart) {      
			Bizweb.updateCartFromForm(cart, '.top-cart-content .mini-products-list');    
		});
	});
</script>
	<!--<a href="#" id="toTop"><span id="toTopHover"></span></a>-->
	<!--	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5565f9cf142bb517"></script>-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>