
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="vi"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="vi"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="vi"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>
		Website Bán Thời Trang 
	</title>
	
	<meta name="description" content="Big Shoe - Thế giới gi&#224;y của bạn">
	
	<meta name="keywords" content="dkt, bizweb, theme, BigShoe theme"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="https://big-shoe.bizwebvietnam.net"/>
	<meta name='revisit-after' content='1 days' />
	<meta name="robots" content="noodp,index,follow" />
	<meta http-equiv="content-language" content="vi" />
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="shortcut icon" href="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/favicon.png?1478571297513" type="image/x-icon" />
	<script src="{!! url('public/web_thoitrang/js/jquery_1.9.1.min.js') !!}" type='text/javascript'></script> 
<!--<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jquery-ui.js?1478571297513' type='text/javascript'></script>-->
<script src="{!! url('public/web_thoitrang/js/jssocials.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/owl.carousel.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/bootstrap.min.js') !!}" type='text/javascript'></script> 
  
	
<link href="{!! url('public/web_thoitrang/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/blogmate.css') !!}" rel='stylesheet' type='text/css' />
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/flexslider.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/owl.carousel.css') !!}" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="{!! url('public/web_thoitrang/css/font-awesome.min.css') !!}">
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome.min.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/jgrowl.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/style.css') !!}" rel='stylesheet' type='text/css' />
	<script>
var Bizweb = Bizweb || {};
Bizweb.store = 'big-shoe.bizwebvietnam.net';
Bizweb.theme = {"id":195294,"name":"BigShoe 0211","role":"main","previewable":true,"processing":false,"created_on":"2016-11-02T10:46:25Z","modified_on":"2016-11-02T10:46:59Z"}
Bizweb.template = 'index';
</script>

                <script>
                //<![CDATA[
                      (function() {
                        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
                        s.src = '//bizweb.dktcdn.net/assets/themes_support/bizweb_stats.js?v=8';
                        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
                      })();

                //]]>
                </script>
<noscript><iframe height='0' width='0' style='display:none;visibility:hidden' src='/visit/record.gif?p=%2f&r=https%3a%2f%2fthemes.bizweb.vn%2fdemo%2fbig-shoe&s=hrqnsaovqqdrw314rgwr55u1'></iframe></noscript>

<script>
(function() {
function asyncLoad() {
for (var i = 0; i < urls.length; i++) {
var s = document.createElement('script');
s.type = 'text/javascript';
s.async = true;
s.src = urls[i];
s.src = urls[i];
var x = document.getElementsByTagName('script')[0];
x.parentNode.insertBefore(s, x);
}
}
window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
})();
</script>

<script type='text/javascript'>
(function() {
var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
log.src = '//stats.bizweb.vn/delivery/91132.js?lang=vi';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
})();
</script>

<!-- Google Tag Manager -->
<noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
</noscript>
<script>
(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
</script>
<!-- End Google Tag Manager -->



	<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
	<!--[if IE 7]>
<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome-ie7.css?1478571297513' rel='stylesheet' type='text/css' />
<![endif]-->
	<!--[if lt IE 9]>
<script src='//html5shiv.googlecode.com/svn/trunk/html5.js' type='text/javascript'></script>
<![endif]-->

	
	<link href="{!! url('public/web_thoitrang/css/cf-stylesheet.css') !!}" rel='stylesheet' type='text/css' />
	<script>var ProductReviewsAppUtil=ProductReviewsAppUtil || {};</script>
</head>
<body id="giay-thoi-trang-nu" class="  cms-index-index cms-home-page" >  
	<header>
	@include('web_thoitrang.blocks.header')
</header>

<nav class="hidden-xs">
@include('web_thoitrang.blocks.nav')
</nav>  
	<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="inner">
				<ul>
					<li class="home"> <a title="Quay lại trang chủ" href="/">Trang chủ</a></li>
					
					              
					<li class="fa fa-angle-double-right" aria-hidden="true"></i>
					@foreach($htdanhmucsp as $dm)
					<li><span class="brn">{!! $dm->tendmsp !!}</span></li>
					@endforeach
					
					
					
				</ul>
			</div>
		</div>
	</div>
</div>

<section class="collection_all">
	<div class="container">
		<aside class="col-lg-3 col-md-4 col-sm-4 hidden-xs sidebar-collection">
	<div class="product_list block">
	<div class="block-title">
		<h5><a href="collections/all"><i class="fa fa-bars" aria-hidden="true">&nbsp;</i>Danh mục sản phẩm</a></h5>
	</div>
	<div class="block-content">
	<?php
		$hienthidm = DB::table('danhmuc')->select('id','tendm')->get();
	?>
	@foreach($hienthidm as $dm)
		<li class="level0 parent drop-menu "><a href="{!! url('web_thoitrang/quanly/danh-muc-san-pham',[$dm->tendm,$dm->id]) !!}"><i class="fa fa-caret-right" aria-hidden="true"></i><span>{!! $dm->tendm !!}</span><i class="fa fa-angle-right" aria-hidden="true"></i></a>			  
			
			<ul class="level1">
				<?php
					$hienthidmsp = DB::table('danhmucsanpham')->select('id','tendmsp','id_dm')->where('id_dm',$dm->id)->get();
				?>
			@foreach($hienthidmsp as $dmsp)
				<li class="level1"><a href="{!! url('web_thoitrang/quanly/sanpham',[$dmsp->tendmsp,$dmsp->id]) !!}"><span>{!! $dmsp->tendmsp !!}</span></a></li>
				@endforeach
			</ul>
			
		</li>
		@endforeach
		<script>
			var li_length = $('.block-content li.level0').length;

			$(document).ready(function(){
				if (li_length <=6 ){
					$(".xemthem").hide();
				} else if (li_length >= 7){
					$(".xemthem").show();
				}
				$(".xemthem").click(function(){
					$(".xemthem").hide();
					$(".display_dinao").show();
				});
				$(".xoadi").click(function (){
					$(".display_dinao").hide();
					$(".xemthem").show();
				});
			});
		</script>
	</div>
</div>

<div class="best_product block">
	<div class="block-title">
		<h5>Sản Phẩm Mới Nhất</h5>
		<div class="hot_sale_navigator">
			<a class="btn prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
			<a class="btn next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
		</div>
	</div>
	<div class="block-content">
		<div class="owl_hot_sale" class="owl-carousel owl-theme">
			<div class="item"> 
			<?php
				$hienthisp = DB::table('sanpham')->select('id','tensp','hinhanh','soluong','dongia')->orderBy('id','DESC')->skip(0)->take(5)->get();
			?>
			@foreach($hienthisp as $sp)	
				<div class="item item_pd">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 item-img">
		
		<a href="{!! url('web_thoitrang/quanly/chi-tiet-san-pham',[$sp->tensp,$sp->id]) !!}"><img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" width="81" height="81" alt="Giày thể thao Converse 4"></a>
		
	</div>
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7 item-info">
		<p class="item-name"><a href="{!! url('web_thoitrang/quanly/chi-tiet-san-pham',[$sp->tensp,$sp->id]) !!}">{!! $sp->tensp !!}</a></p>
		
		<p class="item-price cl_price fs16"><span>{!! number_format($sp->dongia) !!} VND</span></p>
		
	</div>
</div>	
@endforeach
			</div>
			
		</div>
	</div>
</div>



	
<div class="quangcao block">
	<img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/banner.jpg?1478571297513" alt="Quảng cáo"/>
</div>

</aside>

		<article class="col-lg-9 col-md-8 col-sm-8 col-xs-12 article collection">
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collection_header">
				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
			@foreach($htdanhmucsp as $dm)
					<h2 class="txt_u fw_600 pull-left">{!! $dm->tendmsp !!}</h2>
					@endforeach
				</div>
				<div class="toolbar">
					<div class="sorter">
						<div class="view-mode"> 
							<span title="Lưới" class="collection_btn btn_grid active"><i class="fa fa-th" aria-hidden="true"></i></span>
							<span title="Danh sách" class="collection_btn btn_list"><i class="fa fa-th-list" aria-hidden="true"></i></span>
						</div>
					</div>
					<div id="sort-by">
						<label class="left">Sắp xếp: </label>
						<form class="form-inline form-viewpro">
							<div class="form-group">
								<select class="sort-by-script" id="">
									<option value="default">Mặc định</option>
									<option value="price-asc">Giá tăng dần</option>
									<option value="price-desc">Giá giảm dần</option>
									<option value="alpha-asc">Từ A-Z</option>
									<option value="alpha-desc">Từ Z-A</option>
									<option value="created-asc">Cũ đến mới</option>
									<option value="created-desc">Mới đến cũ</option>
								</select>
								<script>
									$(document).ready(function() {
										Bizweb.queryParams = {};
										if (location.search.length) {
											for (var aKeyValue, i = 0, aCouples = location.search.substr(1).split('&'); i < aCouples.length; i++) {
												aKeyValue = aCouples[i].split('=');
												if (aKeyValue.length > 1) {
													Bizweb.queryParams[decodeURIComponent(aKeyValue[0])] = decodeURIComponent(aKeyValue[1]);
												}
											}
										}
										$('.sort-by-script')
											.val('created-desc')
											.bind('change', function() {
											Bizweb.queryParams.sortby = jQuery(this).val();
											location.search = jQuery.param(Bizweb.queryParams).replace(/\+/g, '%20');
										});
									});
								</script>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collection_loop grid-items">
				<!-- SAN PHAM -->
				@foreach($sanpham as $sp)
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 loop-grid">
					
<div class="col-item product-loop-grid bd_red">
	
	
	
	<div class="item-inner">
		<div class="product-wrapper">
			<div class="thumb-wrapper loop-img">
				<a href="{!! url('web_thoitrang/quanly/chi-tiet-san-pham',[$sp->tensp,$sp->id]) !!}" class="thumb flip">
					<span class="face">
						<img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" title="Giày Converse Madison Mono" alt="Giày Converse Madison Mono">
					</span>
					
					
					
					<span class="face back">
						<img src="{!! url('resources/upload/'.$sp->hinhanh) !!}" title="Giày Converse Madison Mono" alt="Giày Converse Madison Mono">
					</span>
					
				</a>
				<div class="view_buy hidden-xs hidden-sm">
					
					<div class="actions">
                        <form action="/cart/add" method="post" class="variants" id="product-actions-2815485" enctype="multipart/form-data">
                            
                            <input type="hidden" name="variantId" value="4566585" />
                            <a href="{!! url('mua-hang',[$sp->tensp,$sp->id]) !!}">Mua ngay</a>
                            
                        </form>
                    </div>
					
					
				</div>
			</div>
		</div>
		<div class="item-info">
			<div class="info-inner">
				<h3 class="item-title"> <a href="/giay-converse-madison-mono-leather" title="Giày Converse Madison Mono">{!! $sp->tensp !!}</a> </h3>
				<div class="item-content">
					<div class="item-price">
						
						
						<div class="price-box"> 
							
							<p class="special-price"> 
								<span class="price-label">Giá khuyến mại</span> 
								<span class="price">{!! $sp->dongia !!}</span> 
							</p>
							<p class="old-price"> 
								<span class="price-label">Giá cũ:</span> 
								<span class="price" id="old-price">{!! $sp->dongiakm !!}</span> 
							</p>
							
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
				</div>

				@endforeach


				<!--  END sanp ham -->
			</div>

			<div class="paginate-pages">
				
			</div>
			
		</article>
	</div>
</section>
	<footer class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="container">
		<div class="row">
			
			<section class="footer-up hidden-xs">
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Thông tin công ty</h5>
					
					<li><a href="/huo-ng-da-n" title="Thông tin">Thông tin</a></li>
					
					<li><a href="/gioi-thieu" title="Giới thiệu công ty">Giới thiệu công ty</a></li>
					
					<li><a href="/" title="Hệ thống các siêu thị">Hệ thống các siêu thị</a></li>
					
					<li><a href="/" title="Tuyển dụng">Tuyển dụng</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Hỗ trợ khách hàng</h5>
					
					<li><a href="/huo-ng-da-n" title="Liên hệ và góp ý">Liên hệ và góp ý</a></li>
					
					<li><a href="/huo-ng-da-n" title="Hướng dẫn mua hàng online">Hướng dẫn mua hàng online</a></li>
					
					<li><a href="/huo-ng-da-n" title="Hướng dẫn mua hàng trả góp">Hướng dẫn mua hàng trả góp</a></li>
					
					<li><a href="/die-u-khoa-n-di-ch-vu" title="Quy chế quản lý hoạt động">Quy chế quản lý hoạt động</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<h5>Chính sách mua hàng</h5>
					
					<li><a href="/die-u-khoa-n-di-ch-vu" title="Quy định, chính sách">Quy định, chính sách</a></li>
					
					<li><a href="/chi-nh-sa-ch" title="Chính sách bảo hành - Đổi trả">Chính sách bảo hành - Đổi trả</a></li>
					
					<li><a href="/chi-nh-sa-ch" title="Chính sách hội viên">Chính sách hội viên</a></li>
					
					<li><a href="/huo-ng-da-n" title="Giao hàng và lắp đặt">Giao hàng và lắp đặt</a></li>
					
				</div>
				<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
					<div class="fb-page" data-href="http://www.facebook.com/facebook" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="http://www.facebook.com/facebook">
								<a href="http://www.facebook.com/facebook">Facebook</a>
							</blockquote>
						</div>
					</div>
				</div>
			</section>
			

			<section class="hidden-lg hidden-md hidden-sm col-xs-12 footer-mobile">
				<ul class="topnav">
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Thông tin công ty</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Thông tin</span> </a></li>
							
							
							
							<li class="level1"> <a href="/gioi-thieu"> <span>Giới thiệu công ty</span> </a></li>
							
							
							
							<li class="level1"> <a href="/"> <span>Hệ thống các siêu thị</span> </a></li>
							
							
							
							<li class="level1"> <a href="/"> <span>Tuyển dụng</span> </a></li>
							
							
						</ul>
					</li>
					
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Hỗ trợ khách hàng</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Liên hệ và góp ý</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Hướng dẫn mua hàng online</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Hướng dẫn mua hàng trả góp</span> </a></li>
							
							
							
							<li class="level1"> <a href="/die-u-khoa-n-di-ch-vu"> <span>Quy chế quản lý hoạt động</span> </a></li>
							
							
						</ul>
					</li>
					
					
					
					<li class="level0 level-top parent"><a class="level-top" href="/"> <span>Chính sách mua hàng</span> </a>
						<ul class="level0">
							
							
							<li class="level1"> <a href="/die-u-khoa-n-di-ch-vu"> <span>Quy định, chính sách</span> </a></li>
							
							
							
							<li class="level1"> <a href="/chi-nh-sa-ch"> <span>Chính sách bảo hành - Đổi trả</span> </a></li>
							
							
							
							<li class="level1"> <a href="/chi-nh-sa-ch"> <span>Chính sách hội viên</span> </a></li>
							
							
							
							<li class="level1"> <a href="/huo-ng-da-n"> <span>Giao hàng và lắp đặt</span> </a></li>
							
							
						</ul>
					</li>
					
					
				</ul>
				<div class="col-lg-3 col-md-3 hidden-sm col-xs-12">
					<div class="fb-page" data-href="http://www.facebook.com/facebook" data-tabs="timeline" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="http://www.facebook.com/facebook">
								<a href="http://www.facebook.com/facebook">Facebook</a>
							</blockquote>
						</div>
					</div>
				</div>
			</section>

			<section class="footer-mid">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-mid-1">
					<h5>Hỗ trợ thanh toán</h5>
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-1.jpg?1478571297513" height="40" width="65" alt="Master Card" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-2.jpg?1478571297513" height="40" width="65" alt="Visa" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-3.jpg?1478571297513" height="40" width="65" alt="Tiền mặt" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-4.jpg?1478571297513" height="40" width="65" alt="Chuyển khoản" />
					<img src="//bizweb.dktcdn.net/thumb/small/100/091/132/themes/195294/assets/payment-5.jpg?1478571297513" height="40" width="65" alt="ATM nội địa">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-mid-2">
					<h5>Giải đáp thắc mắc</h5>
					<div class="tuvan">
						<img src="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/tuvan.png?1478571297513" alt="Tư vấn"/>
					</div>
					<div class="f-mid-2-1">
						<p>Tư vấn miễn phí (24/7)</p>
						<p>1900 6750</p>
					</div>
					<div class="f-mid-2-2">
						<p>Góp ý, phản ánh (8h00 - 20h00)</p>
						<p>1900  6750</p>
					</div>
				</div>
			</section>
			<section class="footer-low">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-low-1">
					<h5>Công ty cổ phần Công nghệ DKT</h5>
					<p>Trụ sở chính: Tầng 4 - Tòa nhà Hanoi Group - 442 Đội Cấn - Ba Đình - Hà Nội</p>
					<p>Điện thoại: HN - (04) 6674 2332 - (04) 3786 8904</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-low-2">
					<p>VPDD: Lầu 3 - Tòa nhà Lữ Gia - Số 70 Lữ Gia - P.15 - Q.11 - TP HCM</p>
					<p>HCM - (08) 6680 9686 - (08) 3866 6276</p>
				</div>
			</section>
		</div>
	</div>
</footer>
<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 copyright">
	<div class="container">
		<div class="row mg0">
			<span>© Bản quyền thuộc về Avent Team </span>
			<br class="hidden-lg hidden-md hidden-sm">
			<span class="hidden-xs">&nbsp;|&nbsp;</span>
			<span> Cung cấp bởi <a href="https://www.bizweb.vn/?utm_source=site-khach-hang&utm_campaign=referral_bizweb&utm_medium=footer&utm_content=cung-cap-boi-bizweb" target="_blank" title="Bizweb" style="color:#ec5d5d;">Bizweb</a></span>
		</div>
	</div>
</section>
	<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/common.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jquery.flexslider.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/cloud-zoom.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/owl.carousel.min.js?1478571297513' type='text/javascript'></script> 
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/parallax.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/assets/themes_support/api.jquery.js?2' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jgrowl.js?1478571297513' type='text/javascript'></script>
<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/cs.script.js?1478571297513' type='text/javascript'></script>
	<script type="text/javascript">
	Bizweb.updateCartFromForm = function(cart, cart_summary_id, cart_count_id) {
		if ((typeof cart_summary_id) === 'string') {
			var cart_summary = jQuery(cart_summary_id);
			if (cart_summary.length) {
				// Start from scratch.
				cart_summary.empty();
				// Pull it all out.        
				jQuery.each(cart, function(key, value) {
					if (key === 'items') {
						var table = jQuery(cart_summary_id);           
						if (value.length) {           
							jQuery.each(value, function(i, item) {	
								var link_img1 = Bizweb.resizeImage(item.image, 'small');
								if(
									link_img1 =="null" || link_img1 =='' || link_img1 == null){
									link_img1 = 'https://bizweb.dktcdn.net/thumb/large/assets/themes_support/noimage.gif';
								}
								jQuery('<li class="item"><a class="product-image" href="' + item.url + '" title="' + item.name + '"><img alt="'+  item.name  + '" src="' + link_img1 +  '"width="'+ '80' +'"\></a><div class="detail-item"><div class="product-details"> <a href="javascript:void(0);" title="Xóa" onclick="Bizweb.removeItem(' + item.variant_id + ')" class="fa fa-remove">&nbsp;</a><p class="product-name"> <a href="' + item.url + '" title="' + item.name + '">' + item.name + '</a></p></div><div class="product-details-bottom"> <span class="price">' + Bizweb.formatMoney(item.price, "55₫") + '</span> <span class="title-desc">Số lượng:</span> <strong>' +  item.quantity + '</strong> </div></div></li>').appendTo(table);
							}); 
							jQuery('<div style="float:left; height:102px;"></div>').appendTo(table);
							jQuery('<li class="cart_fix_1"><div class="top-subtotal">Tổng cộng: <span class="price">' + Bizweb.formatMoney(cart.total_price, "55₫") + '</span></div></li>').appendTo(table);
							jQuery('<li class="cart_fix_2" style="margin-left:-15px;margin-right:-15px;"><div class="actions"><button class="btn-checkout" type="button" onclick="window.location.href=\'/checkout\'"><span>Thanh toán</span></button><button class="view-cart" type="button" onclick="window.location.href=\'/cart\'" ><span>Giỏ hàng</span></button></div></li>').appendTo(table);
						}
						else {
							jQuery('<li class="item"><p>Không có sản phẩm nào trong giỏ hàng.</p></li>').appendTo(table);
						}
					}
				});
			}
		}
		updateCartDesc(cart);
	}
	function updateCartDesc(data){
		var $cartLinkText = $('.mini-cart .cart-box #cart-total, aside.sidebar .block-cart .amount a'),
			$cartPrice = Bizweb.formatMoney(data.total_price, "55₫");		
		switch(data.item_count){
			case 0:
				$cartLinkText.text('0');
				break;
			case 1:
				$cartLinkText.text('1');
				break;
			default:
				$cartLinkText.text(data.item_count);
				break;
		}
		$('.top-cart-content .top-subtotal .price, aside.sidebar .block-cart .subtotal .price').html($cartPrice);
	}  
	Bizweb.onCartUpdate = function(cart) {
		Bizweb.updateCartFromForm(cart, '.top-cart-content .mini-products-list', 'shopping-cart');
	};  
	$(window).load(function() {
		// Let's get the cart and show what's in it in the cart box.  
		Bizweb.getCart(function(cart) {      
			Bizweb.updateCartFromForm(cart, '.top-cart-content .mini-products-list');    
		});
	});
</script>
	<!--<a href="#" id="toTop"><span id="toTopHover"></span></a>-->
	<!--	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5565f9cf142bb517"></script>-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>