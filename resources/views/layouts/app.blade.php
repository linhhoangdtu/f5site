
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="vi"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="vi"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="vi"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>
        Website Bán Thời Trang 
    </title>
    
    <meta name="description" content="Big Shoe - Thế giới gi&#224;y của bạn">
    
    <meta name="keywords" content="dkt, bizweb, theme, BigShoe theme"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="canonical" href="https://big-shoe.bizwebvietnam.net"/>
    <meta name='revisit-after' content='1 days' />
    <meta name="robots" content="noodp,index,follow" />
    <meta http-equiv="content-language" content="vi" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="//bizweb.dktcdn.net/100/091/132/themes/195294/assets/favicon.png?1478571297513" type="image/x-icon" />
    <script src="{!! url('public/web_thoitrang/js/jquery_1.9.1.min.js') !!}" type='text/javascript'></script> 
<!--<script src='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/jquery-ui.js?1478571297513' type='text/javascript'></script>-->
<script src="{!! url('public/web_thoitrang/js/jssocials.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/owl.carousel.min.js') !!}" type='text/javascript'></script> 
<script src="{!! url('public/web_thoitrang/js/bootstrap.min.js') !!}" type='text/javascript'></script> 
  
    
<link href="{!! url('public/web_thoitrang/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/blogmate.css') !!}" rel='stylesheet' type='text/css' />
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/flexslider.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/owl.carousel.css') !!}" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="{!! url('public/web_thoitrang/css/font-awesome.min.css') !!}">
<!--<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome.min.css?1478571297513' rel='stylesheet' type='text/css' />-->
<link href="{!! url('public/web_thoitrang/css/jgrowl.css') !!}" rel='stylesheet' type='text/css' />
<link href="{!! url('public/web_thoitrang/css/style.css') !!}" rel='stylesheet' type='text/css' />
    <script>
var Bizweb = Bizweb || {};
Bizweb.store = 'big-shoe.bizwebvietnam.net';
Bizweb.theme = {"id":195294,"name":"BigShoe 0211","role":"main","previewable":true,"processing":false,"created_on":"2016-11-02T10:46:25Z","modified_on":"2016-11-02T10:46:59Z"}
Bizweb.template = 'index';
</script>

                <script>
                //<![CDATA[
                      (function() {
                        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
                        s.src = '//bizweb.dktcdn.net/assets/themes_support/bizweb_stats.js?v=8';
                        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
                      })();

                //]]>
                </script>
<noscript><iframe height='0' width='0' style='display:none;visibility:hidden' src='/visit/record.gif?p=%2f&r=https%3a%2f%2fthemes.bizweb.vn%2fdemo%2fbig-shoe&s=hrqnsaovqqdrw314rgwr55u1'></iframe></noscript>

<script>
(function() {
function asyncLoad() {
for (var i = 0; i < urls.length; i++) {
var s = document.createElement('script');
s.type = 'text/javascript';
s.async = true;
s.src = urls[i];
s.src = urls[i];
var x = document.getElementsByTagName('script')[0];
x.parentNode.insertBefore(s, x);
}
}
window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
})();
</script>

<script type='text/javascript'>
(function() {
var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
log.src = '//stats.bizweb.vn/delivery/91132.js?lang=vi';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
})();
</script>

<!-- Google Tag Manager -->
<noscript>
<iframe src='//www.googletagmanager.com/ns.html?id=GTM-MS77Z9' height='0' width='0' style='display:none;visibility:hidden'></iframe>
</noscript>
<script>
(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
</script>
<!-- End Google Tag Manager -->



    <!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
    <!--[if IE 7]>
<link href='//bizweb.dktcdn.net/100/091/132/themes/195294/assets/font-awesome-ie7.css?1478571297513' rel='stylesheet' type='text/css' />
<![endif]-->
    <!--[if lt IE 9]>
<script src='//html5shiv.googlecode.com/svn/trunk/html5.js' type='text/javascript'></script>
<![endif]-->

    
    <link href="{!! url('public/web_thoitrang/css/cf-stylesheet.css') !!}" rel='stylesheet' type='text/css' />
    <script>var ProductReviewsAppUtil=ProductReviewsAppUtil || {};</script>
</head>
<body id="big-shoe" class="  cms-index-index cms-home-page" >  
    <header>
    @include('web_thoitrang.blocks.header')
</header>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
