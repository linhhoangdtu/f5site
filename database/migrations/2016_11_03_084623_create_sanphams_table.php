<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSanphamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanpham', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tensp');
            $table->string('hinhanh');
            $table->integer('soluong');
            $table->double('dongia');
            $table->string('mota');
            $table->double('dongiakm');
            $table->integer('id_dm')->unsigned();
            $table->foreign('id_dm')->references('id')->on('danhmuc')->onDelete('cascade');
            $table->integer('id_dmsp')->unsigned();
            $table->foreign('id_dmsp')->references('id')->on('danhmucsanpham')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanpham');
    }
}
