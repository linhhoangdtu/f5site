<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageHienthisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_hienthi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hinhanhht');
            $table->integer('id_dm')->unsigned();
            $table->foreign('id_dm')->references('id')->on('danhmuc')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_hienthi');
    }
}
