<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('/home');
});
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	Route::group(['prefix'=>'danhmuc'],function (){
		Route::get('list',['as'=>'admin.danhmuc.list','uses'=>'DanhmucController@getlist']);
		Route::get('add',['as'=>'admin.danhmuc.getAdd','uses'=>'DanhmucController@getAdd']);
		Route::post('add',['as'=>'admin.danhmuc.postAdd','uses'=>'DanhmucController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.danhmuc.getDelete','uses'=>'DanhmucController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.danhmuc.getEdit','uses'=>'DanhmucController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.danhmuc.postEdit','uses'=>'DanhmucController@postEdit']);
	});
	Route::group(['prefix'=>'danhmucsanpham'],function (){
		Route::get('list',['as'=>'admin.danhmucsanpham.list','uses'=>'DanhmucsanphamController@getlist']);
		Route::get('add',['as'=>'admin.danhmucsanpham.getAdd','uses'=>'DanhmucsanphamController@getAdd']);
		Route::post('add',['as'=>'admin.danhmucsanpham.postAdd','uses'=>'DanhmucsanphamController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.danhmucsanpham.getDelete','uses'=>'DanhmucsanphamController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.danhmucsanpham.getEdit','uses'=>'DanhmucsanphamController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.danhmucsanpham.postEdit','uses'=>'DanhmucsanphamController@postEdit']);
	});
	Route::get('listdonhang',['as'=>'admin.sanpham.listdonhang','uses'=>'SanphamController@getListdonhang']);
	Route::get('delete/{id}',['as'=>'admin.sanpham.getDeletedonhang','uses'=>'SanphamController@getDeletedonhang']);
	Route::group(['prefix'=>'sanpham'],function(){
		Route::get('list',['as'=>'admin.sanpham.list','uses'=>'SanphamController@getList']);
		Route::get('add',['as'=>'admin.sanpham.getAdd','uses'=>'SanphamController@getAdd']);
		Route::post('add',['as'=>'admin.sanpham.postAdd','uses'=>'SanphamController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.sanpham.getDelete','uses'=>'SanphamController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.sanpham.getEdit','uses'=>'SanphamController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.sanpham.postEdit','uses'=>'SanphamController@postEdit']);
	});
	Route::group(['prefix'=>'user'],function(){
		Route::get('list',['as'=>'admin.user.list','uses'=>'UserController@getList']);
		Route::get('add',['as'=>'admin.user.getAdd','uses'=>'UserController@getAdd']);
		Route::post('add',['as'=>'admin.user.postAdd','uses'=>'UserController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.user.getDelete','uses'=>'UserController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.user.getEdit','uses'=>'UserController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.user.postEdit','uses'=>'UserController@postEdit']);
	});
	Route::group(['prefix'=>'khachhang'],function(){
		Route::get('list',['as'=>'admin.khachhang.list','uses'=>'KhachhangController@getList']);
		Route::get('add',['as'=>'admin.khachhang.getAdd','uses'=>'KhachhangController@getAdd']);
		Route::post('add',['as'=>'admin.khachhang.postAdd','uses'=>'KhachhangController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.khachhang.getDelete','uses'=>'KhachhangController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.khachhang.getEdit','uses'=>'KhachhangController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.khachhang.postEdit','uses'=>'KhachhangController@postEdit']);
	});

});
Route::get('loaidanhmuc/{id}/{tendm}',['as'=>'loaidm','uses'=>'WebcomeController@loaidm']);
Route::get('loaisanpham/{id}/{tendmsp}',['as'=>'loaidmsp','uses'=>'WebcomeController@loaidmsp']);
Route::group(['prefix'=>'web_thoitrang'],function(){
	Route::group(['prefix'=>'quanly'],function(){
	
	Route::get('danh-muc-san-pham/{tendm}/{id}',['as'=>'getDanhmucsanpham','uses'=>'Controller@getDanhmucsanpham']);
	Route::get('trangsanpham',['as'=>'getTrangsanpham','uses'=>'Controller@getTrangsanpham']);
	Route::get('sanpham/{tendmsp}/{id}',['as'=>'getSanpham','uses'=>'Controller@getSanpham']);
	Route::get('chi-tiet-san-pham/{tensp}/{id}',['as'=>'getChitietsanpham','uses'=>'Controller@getChitietsanpham']);
	});

});
	// Route::get('login',['as'=>'getLogin','uses'=>'LoginController@getLogin']);
	// Route::post('login',['as'=>'postLogin','uses'=>'LoginController@postLogin']);
	// Route::get('register',['as'=>'getRegister','uses'=>'LoginController@getRegister']);
	// Route::post('register',['as'=>'postRegister','uses'=>'LoginController@postRegister']);
Route::get('search','Controller@getSearch');
	Route::get('mua-hang/{tensp}/{id}',['as'=>'getMuahang','uses'=>'Controller@getMuahang']);
	Route::get('gio-hang',['as'=>'getGiohang','middleware'=>'auth','uses'=>'Controller@getGiohang']);
	Route::get('xoa-san-pham/{id}',['as'=>'xoasanpham','uses'=>'Controller@xoasanpham']);
	Route::get('cap-nhat/{id}/{qty}',['as'=>'capnhatsanpham','uses'=>'Controller@capnhatsanpham']);
	Route::get('thanh-toan',['as'=>'getThanhtoan','uses'=>'Controller@getThanhtoan']);
	Route::post('thanh-toan',['as'=>'postThanhtoan','uses'=>'Controller@postThanhtoan']);

Auth::routes();
